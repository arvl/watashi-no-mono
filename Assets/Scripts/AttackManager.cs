﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttackManager : MonoBehaviour {
    #region References
    public PlayerController player;
    public RectTransform knifeRoot;
    public RectTransform[] knife;
    public RectTransform killminigame_root;
    public AudioSource killminigame_audio;
    public CanvasGroup killminigame_group;
    public Image LogoImage;
    public Sprite[] logoSprites;
    public Animator logoAnimator;
    public Text logoKeyText;
    public bool killminigame_enabled;
    int killminigame_currentKnife;
    bool killminigame_stabbed;
    bool killminigame_won;
    #endregion

    // Run every frame
    void Update()
    {
        updateSlowMoSprites();
        updateAttackPosition();
    }

    // Attack the selected student
    void updateAttackPosition()
    {
        // Move the student to the correct spot
        if (player.stats.playerState == playerStatus.Attacking && player.stats.selectedStudent != null)
        {
            player.stats.selectedStudent.transform.position = Vector3.Lerp(player.stats.selectedStudent.transform.position, player.transform.position + (player.transform.forward * player.references.attackPositionOffsets[player.stats.itemEquipped].z), Time.deltaTime * 8);
            player.stats.selectedStudent.transform.rotation = Quaternion.Lerp(player.stats.selectedStudent.transform.rotation, Quaternion.LookRotation(player.transform.position - player.stats.selectedStudent.transform.position), Time.deltaTime * 8);
        }
    }

    // Play the attack event
    public IEnumerator attackEnum()
    {
        player.stats.playerState = playerStatus.Attacking;
        player.stats.selectedStudent.character.characterCollider.enabled = false;
        player.stats.selectedStudent.character.characterBody.isKinematic = true;
        player.stats.canMove = playerMovementMode.cameraOnly;
        player.stats.promptsEnabled = false;
        player.stats.selectedStudent.character.lookTarget = player.transform;
        player.transform.LookAt(player.stats.selectedStudent.transform.position);
        if (!player.references.policeManager.weaponsUsed.Contains(player.references.inventoryManager.selected.myItem))
            player.references.policeManager.weaponsUsed.Add(player.references.inventoryManager.selected.myItem);
        switch (player.stats.itemEquipped)
        {
            case 1:
                #region Knife Attack
                player.showUI = false;
                SoundManager.Play("KnifeAttack", player.references.audioSource, PlayerPrefs.GetFloat("effectVolume"));
                player.playAnimation("knifeAttack_A", 0.2f);
                player.stats.selectedStudent.character.playAnimation("knifeAttack_B", 0.1f);
                player.stats.selectedStudent.character.playFaceAnimation("Face_Scared", 0.5f);
                player.stats.selectedStudent.character.status = characterStatus.Dead;
                player.stats.selectedStudent.Scream();
                startSlowmoMinigame();
                yield return new WaitForSeconds(0.7f);
                killminigame_enabled = false;
                player.references.animator.speed = 1;
                player.stats.selectedStudent.character.animator.speed = 1;
                player.stats.sanity -= 0.8f;
                if (!killminigame_won)
                {
                    SoundManager.Play("KnifeAttack", player.references.audioSource, PlayerPrefs.GetFloat("effectVolume"));
                    player.playAnimation("knifeAttack_A_Fail", 0.2f);
                    player.stats.selectedStudent.character.playAnimation("knifeAttack_B_Fail", 0.1f);
                    yield return new WaitForSeconds(2f);
                    startSlowmoMinigame();
                    yield return new WaitForSeconds(0.5f);
                    killminigame_enabled = false;
                    player.references.animator.speed = 1;
                    player.stats.selectedStudent.character.animator.speed = 1;
                    player.stats.sanity -= 0.8f;
                    if (killminigame_won)
                    {
                        player.playAnimation("knifeAttack_A_Dodge", 0.2f);
                        SoundManager.Play("KnifeAttack_Fail_Dodge", player.references.audioSource, PlayerPrefs.GetFloat("effectVolume"));
                        yield return new WaitForSeconds(2f);
                        StartCoroutine(attackEnum());
                    }
                    else
                    {
                        yield return new WaitForSeconds(1f);
                        checkKillMinigame();
                        player.stats.playerState = playerStatus.Nothing;
                        yield return new WaitForSeconds(2.4f);
                        player.showUI = true;
                    }
                }
                else
                {
                    yield return new WaitForSeconds(3f);
                    player.stats.playerState = playerStatus.Nothing;
                    checkKillMinigame();
                    yield return new WaitForSeconds(0.4f);
                    player.showUI = true;
                }
                break;
            #endregion
            case 3:
                #region Pipe Attack
                player.showUI = false;
                SoundManager.Play("PipeAttack", player.references.audioSource, PlayerPrefs.GetFloat("effectVolume"));
                player.references.audioSource.time = 0;
                player.playAnimation("pipeAttack_A", 0.2f);
                player.stats.selectedStudent.character.playAnimation("pipeAttack_B", 0.1f);
                player.stats.selectedStudent.character.playFaceAnimation("Face_Scared", 0.5f);
                player.stats.selectedStudent.character.status = characterStatus.Dead;
                player.stats.selectedStudent.Scream();
                startSlowmoMinigame();
                yield return new WaitForSeconds(0.7f);
                killminigame_enabled = false;
                player.references.animator.speed = 1;
                player.stats.selectedStudent.character.animator.speed = 1;
                player.stats.sanity -= 0.8f;
                if (!killminigame_won)
                {
                    player.references.audioSource.Stop();
                    SoundManager.Play("KnifeAttack_Fail", player.references.audioSource, PlayerPrefs.GetFloat("effectVolume"));
                    player.playAnimation("knifeAttack_A_Fail", 0.2f);
                    player.stats.selectedStudent.character.playAnimation("knifeAttack_B_Fail", 0.1f);
                    yield return new WaitForSeconds(2f);
                    startSlowmoMinigame();
                    yield return new WaitForSeconds(0.5f);
                    killminigame_enabled = false;
                    player.references.animator.speed = 1;
                    player.stats.selectedStudent.character.animator.speed = 1;
                    player.stats.sanity -= 0.8f;
                    if (killminigame_won)
                    {
                        player.playAnimation("knifeAttack_A_Dodge", 0.2f);
                        SoundManager.Play("KnifeAttack_Fail_Dodge", player.references.audioSource, PlayerPrefs.GetFloat("effectVolume"));
                        yield return new WaitForSeconds(2f);
                        StartCoroutine(attackEnum());
                    }
                    else
                    {
                        yield return new WaitForSeconds(1f);
                        checkKillMinigame();
                        player.stats.playerState = playerStatus.Nothing;
                        yield return new WaitForSeconds(2.4f);
                        player.showUI = true;
                    }
                }
                else
                {
                    yield return new WaitForSeconds(0.5f);
                    startSlowmoMinigame();
                    yield return new WaitForSeconds(0.5f);
                    killminigame_enabled = false;
                    player.references.animator.speed = 1;
                    player.stats.selectedStudent.character.animator.speed = 1;
                    if (killminigame_won)
                    {
                        yield return new WaitForSeconds(0.25f);
                        player.references.audioSource.time = 1.55f;
                        player.stats.selectedStudent.character.faceMesh.material = player.references.studentDamageMaterials[0];
                        player.stats.selectedStudent.Head.GetComponent<Rigidbody>().velocity = (player.transform.forward * 15) + (player.transform.right * 25);
                        player.stats.selectedStudent.character.playFaceAnimation("Face_Sleep", 0.25f);
                        player.stats.selectedStudent.character.enableRagdoll();
                        studentFX(0);
                        yield return new WaitForSeconds(0.5f);
                    }
                    else
                    {
                        player.stats.selectedStudent.character.status = characterStatus.Unconsious;
                        player.stats.selectedStudent.character.enableRagdoll();
                        player.stats.selectedStudent.Head.GetComponent<Rigidbody>().velocity = (player.transform.forward * 10) - (player.transform.right * 15);
                        player.stats.selectedStudent.character.playFaceAnimation("Face_Sleep", 0.25f);
                    }
                    player.stats.playerState = playerStatus.Nothing;
                    player.showUI = true;
                }
                #endregion
                break;
        }
        if (player.stats.playerState == playerStatus.Nothing)
        {
            player.stats.canMove = playerMovementMode.all;
            player.stats.promptsEnabled = true;
            player.playAnimation(player.moveAnims.idleAnim, 0.5f);
            player.stats.bloodiness++;
            player.stats.selectedStudent = null;
        }
    }

    // Make the selected student spray blood
    public void studentFX(int effect)
    {
        player.stats.selectedStudent.damageEffects[effect].Play();
        player.stats.bloodiness++;
    }

    // Check if the player won the slowmo minigame or not
    void checkKillMinigame()
    {
        if (killminigame_won)
        {
            player.stats.selectedStudent.character.enableRagdoll();
            player.stats.playerState = playerStatus.Nothing;
            player.stats.selectedStudent.character.playFaceAnimation("Face_Sleep", 0.5f);
        }
        else
        {
            player.stats.selectedStudent.character.status = characterStatus.Scared;
            player.stats.selectedStudent.character.animator.speed = 1;
            player.stats.selectedStudent.character.setDestination(player.stats.selectedStudent.homeSpot, true);
        }
    }

    // Update the sprites in the slowmo minigame
    void updateSlowMoSprites()
    {
        if (killminigame_enabled)
        {
            if (!player.stats.currentAnimation.Contains("Fail"))
            {
                killminigame_root.position = player.references.mainCamera.cam.WorldToScreenPoint(player.stats.selectedStudent.Head.parent.position);
            }
            else
            {
                killminigame_root.position = player.references.mainCamera.cam.WorldToScreenPoint(player.references.Hips.position);
            }
            killminigame_group.alpha = Mathf.Lerp(killminigame_group.alpha, 0.85f, Time.deltaTime * 8);
            player.references.animator.speed = Mathf.Lerp(player.references.animator.speed, 0, Time.deltaTime * 1);
            player.stats.selectedStudent.character.animator.speed = Mathf.Lerp(player.stats.selectedStudent.character.animator.speed, 0, Time.deltaTime * 1);
            knifeRoot.Rotate(0, 0, 150 * Time.deltaTime);
            logoKeyText.text = player.stats.keyboard ? "F" : "△";
            LogoImage.sprite = logoSprites[killminigame_currentKnife];
            if (killminigame_currentKnife < 6)
            {
                if (Input.GetButtonDown("Attack"))
                {
                    killminigame_currentKnife++;
                    SoundManager.Play("YS_KnifeStab1", PlayerPrefs.GetFloat("effectVolume"));
                    logoAnimator.CrossFadeInFixedTime("UI_Shake", 0.1f);
                }
            }
            else
            {
                killminigame_won = true;
            }
            knife[0].localPosition = killminigame_currentKnife >= 1 ? Vector3.Lerp(knife[0].localPosition, Vector3.zero, Time.deltaTime * 100) : new Vector3(181.3f, 28.65f, 0f);
            knife[1].localPosition = killminigame_currentKnife >= 2 ? Vector3.Lerp(knife[1].localPosition, Vector3.zero, Time.deltaTime * 100) : new Vector3(181.3f, 28.65f, 0f);
            knife[2].localPosition = killminigame_currentKnife >= 3 ? Vector3.Lerp(knife[2].localPosition, Vector3.zero, Time.deltaTime * 100) : new Vector3(181.3f, 28.65f, 0f);
            knife[3].localPosition = killminigame_currentKnife >= 4 ? Vector3.Lerp(knife[3].localPosition, Vector3.zero, Time.deltaTime * 100) : new Vector3(181.3f, 28.65f, 0f);
            knife[4].localPosition = killminigame_currentKnife >= 5 ? Vector3.Lerp(knife[4].localPosition, Vector3.zero, Time.deltaTime * 100) : new Vector3(181.3f, 28.65f, 0f);
            knife[5].localPosition = killminigame_currentKnife >= 6 ? Vector3.Lerp(knife[5].localPosition, Vector3.zero, Time.deltaTime * 100) : new Vector3(181.3f, 28.65f, 0f);
        }
        else
        {
            killminigame_group.alpha = Mathf.Lerp(killminigame_group.alpha, -0.5f, Time.deltaTime * 8);
        }
    }

    // Start the slowmo minigame
    void startSlowmoMinigame()
    {
        Time.timeScale = 1f;
        killminigame_audio.Play();
        killminigame_stabbed = false;
        killminigame_enabled = true;
        killminigame_won = false;
        killminigame_currentKnife = 0;
    }
}
