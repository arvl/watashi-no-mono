﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Item : MonoBehaviour {
    public Collider itemCollider;
    public Prompt myPrompt;
    public Inventory inventoryManager;
    public weaponType WeaponEquipType;
    public AudioSource audioSRC;
    public int itemID;

    void Awake()
    {
        if (!inventoryManager)
        {
            inventoryManager = GameObject.FindObjectOfType<Inventory>();
        }
    }

    public void PickUp()
    {
        if (WeaponEquipType == weaponType.SmallWeapon || WeaponEquipType == weaponType.SmallItem)
        {
            int slot = inventoryManager.getEmptySlot();
            if (slot != -1)
            {
                inventoryManager.givePlayerItem(transform, itemID);
                inventoryManager.current = slot;
                inventoryManager.slots[slot].myItem = this;
                myPrompt.active = false;
            }
        }
        else
        {
            if (inventoryManager.slots[0].myItem != null)
            {
                inventoryManager.dropItem(0);
            }
            inventoryManager.givePlayerHeavyItem(transform, itemID);
            inventoryManager.current = 0;
            inventoryManager.slots[0].myItem = this;
            myPrompt.active = false;
        }
    }
}