﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoliceTapePole : MonoBehaviour {
    public LineRenderer tapeRenderer;
    public Transform pole2;

	void Update()
    {
        updateTape();
	}

    public void OnDrawGizmos()
    {
        updateTape();
    }

    void updateTape()
    {
        if (tapeRenderer != null && pole2 != null)
        {
            tapeRenderer.enabled = true;
            tapeRenderer.SetPosition(0, transform.position + new Vector3(0, 0.95f, 0));
            tapeRenderer.SetPosition(1, pole2.position + new Vector3(0, 0.95f, 0));
        }
        else
        {
            if (tapeRenderer != null)
            {
                tapeRenderer.enabled = false;
                tapeRenderer.SetPosition(0, transform.position);
                tapeRenderer.SetPosition(1, transform.position);
            }
        }
    }
}
