﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptedEvents : MonoBehaviour {
    public List<scriptedEvent> dayEvents = new List<scriptedEvent>();
    [SerializeField] ClockUI timeManager;
    [SerializeField] PlayerController player;
    public bool cancelEvent;

    void Start () {
        StartCoroutine(startDayScheduel());
	}

    // Start the student's day scheduel
    IEnumerator startDayScheduel()
    {
        while (gameObject.activeInHierarchy)
        {
            // Find a student event to start
            scriptedEvent Event = findEvent(dayEvents);
            int currentMinute = timeManager.currentTime.minute;

            if (Event != null)
            {
                cancelEvent = false;
                checkIfDependentStudentsNormal(Event.necessaryStudents);

                if (!cancelEvent)
                {

                    foreach (eventNecessaryStudents startMovement in Event.necessaryStudents)
                    {
                        startMovement.highlightedStudent.inScriptedEvent = true;
                        startMovement.highlightedStudent.character.setDestination(startMovement.wantedDestination, startMovement.runToDestination);
                        startMovement.highlightedStudent.busy = startMovement.setStudentToBusy == studentEventBusyType.BusyOnWalkingToEvent ? true : false;
                    }

                    while (true)
                    {
                        if (checkIfDependentStudentsarrivedAtDestination(Event.necessaryStudents))
                        {
                            break;
                        }
                        else
                        {
                            yield return new WaitForSeconds(0.5f);
                        }
                    }

                    // Start the event's phases
                    yield return new WaitForSeconds(0.2f);

                    checkIfDependentStudentsNormal(Event.necessaryStudents);

                    foreach (eventNecessaryStudents startMovement in Event.necessaryStudents)
                    {
                        startMovement.highlightedStudent.busy = startMovement.setStudentToBusy == studentEventBusyType.DontSetToBusy ? false : true;
                    }

                    // Start the event phases
                    foreach (eventPhase Phase in Event.Phases)
                    {
                        checkIfDependentStudentsNormal(Event.necessaryStudents);

                        if (!cancelEvent)
                        {
                            Student highlightedStudent = Phase.highlightedStudent;

                            Phase.specialFunctions.Invoke();
                            if (Phase.wantedDestination != null)
                            {
                                highlightedStudent.character.setDestination(Phase.wantedDestination, Phase.runToDestination);
                                yield return new WaitUntil(() => highlightedStudent.character.arrivedAtDestination);
                            }
                            foreach (eventAnimation anim in Phase.AnimationsToPlay)
                            {
                                anim.highlightedStudent.character.playAnimation("", 0.25f);
                                if (anim.wantedDestinationWithoutPhaseDelay == null)
                                {
                                    #region Animation name stuff 
                                    switch (anim.animation)
                                    {
                                        case studentConversationAnimation.Think0:
                                            anim.highlightedStudent.character.playAnimation("Think_00", 0.25f);
                                            break;
                                        case studentConversationAnimation.Greet0:
                                            anim.highlightedStudent.character.playAnimation("Greet_00", 0.25f);
                                            break;
                                        case studentConversationAnimation.Greet1:
                                            anim.highlightedStudent.character.playAnimation("Greet_01", 0.25f);
                                            break;
                                        case studentConversationAnimation.Nod0:
                                            anim.highlightedStudent.character.playAnimation("NodHead_00", 0.25f);
                                            break;
                                        case studentConversationAnimation.Salute0:
                                            anim.highlightedStudent.character.playAnimation("Salute_00", 0.25f);
                                            break;
                                        case studentConversationAnimation.Shake0:
                                            anim.highlightedStudent.character.playAnimation("ShakeHead_00", 0.25f);
                                            break;
                                    }
                                    #endregion
                                }
                                else
                                {
                                    anim.highlightedStudent.character.setDestination(anim.wantedDestinationWithoutPhaseDelay, anim.runToDestination);
                                }
                            }
                            if (Phase.voicedLine != null)
                            {
                                highlightedStudent.mouthMovementScript.enabled = true;
                                SoundManager.Play(Phase.voicedLine.name, highlightedStudent.character.audioSource, Phase.voicedLineVolume * 0.5f);
                                highlightedStudent.character.playFaceAnimation("FaceBlink", 0.25f);
                                Phase.secondsToWait += Phase.voicedLine.length;
                            }
                            if (Phase.subtitleText != "" && Vector3.Distance(player.transform.position, highlightedStudent.transform.position) < highlightedStudent.character.audioSource.maxDistance)
                            {
                                SubtitlesUI.Get().AddText(Phase.subtitleText, Phase.secondsToWait);
                            }
                            yield return new WaitForSeconds(Phase.secondsToWait);
                            highlightedStudent.mouthMovementScript.enabled = false;
                        }
                        else
                        {
                            break;
                        }
                    }

                    // The end of the event
                    foreach (eventNecessaryStudents student in Event.necessaryStudents)
                    {
                        student.highlightedStudent.character.audioSource.Stop();
                        if (student.highlightedStudent.character.status == characterStatus.Normal)
                        {
                            student.highlightedStudent.busy = false;
                            student.highlightedStudent.inScriptedEvent = false;
                            student.highlightedStudent.character.lookTarget = null;

                            if (student.endDestination != null)
                                student.highlightedStudent.character.setDestination(student.endDestination, student.runToEndDestination);
                            else
                                student.highlightedStudent.character.setDestination(student.highlightedStudent.character.lastDestination, student.runToEndDestination);
                        }
                    }
                }
            }

            // Wait for the time to be different before running again
            yield return new WaitUntil(() => timeManager.currentTime.minute != currentMinute);
        }
    }

    scriptedEvent findEvent(List<scriptedEvent> Events)
    {
        scriptedEvent foundEvent = null;

        foreach (scriptedEvent Event in Events)
        {
            if (timeManager.currentTime.MatchTime(Event.eventTime))
            {
                foundEvent = Event;
                break;
            }
        }

        return foundEvent;
    }

    // Check if students are arrivedAtDestination
    bool checkIfDependentStudentsarrivedAtDestination(eventNecessaryStudents[] students)
    {
        bool dependentStudentarrivedAtDestination = true;
        foreach (eventNecessaryStudents student in students)
        {
            if (!student.highlightedStudent.character.arrivedAtDestination && !student.dontWaitForStudent)
            {
                dependentStudentarrivedAtDestination = false;
            }
        }

        return dependentStudentarrivedAtDestination;
    }

    // Check if students are at school
    bool checkIfDependentStudentsExist(eventNecessaryStudents[] students)
    {
        bool dependentStudentExists = true;
        foreach (eventNecessaryStudents student in students)
        {
            if (!student.highlightedStudent.gameObject.activeInHierarchy)
            {
                dependentStudentExists = false;
            }
        }

        return dependentStudentExists;
    }

    // Cancel the event if a necessary student is dead/scared/suspicious
    void checkIfDependentStudentsNormal(eventNecessaryStudents[] students)
    {
        foreach (eventNecessaryStudents student in students)
        {
            if (student.highlightedStudent.character.status != characterStatus.Normal)
            {
                cancelEvent = true;
            }
        }
    }
}
