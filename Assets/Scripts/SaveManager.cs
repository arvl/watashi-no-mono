﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;
using UnityEngine.SceneManagement;

public class SaveManager : MonoBehaviour
{
    public static void saveGame(int saveID)
    {
        string path = Application.streamingAssetsPath + "/Saves/Save" + saveID + ".wnmsave";
        FileStream file;

        if (File.Exists(path))
        {
            File.Delete(path);
            file = File.Create(path);
            file.Close();
        }

        saveData SaveData = new saveData();
        SaveData.studentsKilled = PlayerPrefs.GetInt("StudentsKilled");
        SaveData.playerStrength = PlayerPrefs.GetFloat("PlayerStrength");
        SaveData.playerSpeed = PlayerPrefs.GetFloat("PlayerSpeed");
        SaveData.playerSeduction = PlayerPrefs.GetFloat("PlayerSeduction");
        SaveData.am = PlayerPrefs.GetInt("Night");
        SaveData.playerTired = PlayerPrefs.GetInt("PlayerTired");
        SaveData.week = PlayerPrefs.GetInt("Week");
        SaveData.day = PlayerPrefs.GetInt("DayID");

        foreach (Student student in GameObject.FindObjectsOfType<Student>())
        {
            if (student.metPlayer)
            {
                SaveData.studentsMet.Add(student.name);
            }
        }

        #region Save the evidence in the scene
        PoliceManager policeManager = GameObject.FindObjectOfType<PoliceManager>();
        if (policeManager != null)
        {
            foreach (Student student in policeManager.studentsKilled)
            {
                SaveData.absentStudents.Add(student.name);
            }

            foreach (Item item in policeManager.weaponsUsed)
            {
                if (item.transform.parent == null && item.gameObject.activeInHierarchy)
                {
                    policeEvidenceItem newEvidence = new policeEvidenceItem();
                    newEvidence.type = evidenceType.item;
                    newEvidence.position = item.transform.position;
                    newEvidence.name = item.name;
                    SaveData.evidence.Add(newEvidence);
                }
            }

            foreach (Item item in policeManager.bloodyUniforms)
            {
                policeEvidenceItem newEvidence = new policeEvidenceItem();
                newEvidence.type = evidenceType.item;
                newEvidence.position = item.transform.position;
                newEvidence.name = item.name;
                SaveData.evidence.Add(newEvidence);
            }

            foreach (Student item in policeManager.studentsKilled)
            {
                policeEvidenceItem newEvidence = new policeEvidenceItem();
                newEvidence.type = evidenceType.body;
                newEvidence.position = item.character.ragdoll.Limbs["Hips"].position;
                newEvidence.position.y = Mathf.CeilToInt(newEvidence.position.y);
                SaveData.evidence.Add(newEvidence);
            }

            if (policeManager.savedEvidence != null)
            {
                foreach (policeEvidenceItem item in policeManager.savedEvidence)
                {
                    SaveData.evidence.Add(item);
                }
            }
        }
        #endregion

        string json = JsonUtility.ToJson(SaveData);

        StreamWriter writer = new StreamWriter(path, true);
        writer.Write(json);
        writer.Close();
    }

    public static void loadGame(int saveID)
    {
        saveData SaveData = getSaveData(saveID);

        if (SaveData != null)
        {
            PlayerPrefs.SetInt("StudentsKilled", SaveData.studentsKilled);
            PlayerPrefs.SetFloat("PlayerStrength", SaveData.playerStrength);
            PlayerPrefs.SetFloat("PlayerSpeed", SaveData.playerSpeed);
            PlayerPrefs.SetFloat("PlayerSeduction", SaveData.playerSeduction);
            PlayerPrefs.SetInt("Night", SaveData.am);
            PlayerPrefs.SetInt("PlayerTired", SaveData.playerTired);
            PlayerPrefs.SetInt("Week", SaveData.week);
            PlayerPrefs.SetInt("DayID", SaveData.day);
            PlayerPrefs.SetInt("SaveFileID", saveID);

            foreach (string studentName in SaveData.absentStudents)
            {
                GameObject student = GameObject.Find(studentName);
                if (student != null)
                {
                    student.SetActive(false);
                }
            }

            foreach (Student student in GameObject.FindObjectsOfType<Student>())
            {
                if (SaveData.studentsMet.Contains(student.name))
                {
                    student.metPlayer = true;
                }
            }
        }
    }

    public static saveData getSaveData(int saveID)
    {
        string path = Application.streamingAssetsPath + "/Saves/Save" + saveID + ".wnmsave";
        FileStream file;

        if (!File.Exists(path))
        {
            file = File.Create(path);
            file.Close();
        }

        saveData SaveData;

        StreamReader reader = new StreamReader(path);
        SaveData = JsonUtility.FromJson<saveData>(reader.ReadToEnd());
        reader.Close();
        return SaveData;
    }

    public static void eraseSave(int saveID)
    {
        if (File.Exists(Application.streamingAssetsPath + "/Saves/Save" + saveID + ".wnmsave"))
        {
            File.Delete(Application.streamingAssetsPath + "/Saves/Save" + saveID + ".wnmsave");
        }
    }

    public static string getSaveDate(int saveID)
    {
        string path = Application.streamingAssetsPath + "/Saves/Save" + saveID + ".wnmsave";
        string date = File.GetLastWriteTime(path).ToString();
        return date;
    }

    public static saveStatus saveExists(int saveID)
    {
        string path = Application.streamingAssetsPath + "/Saves/Save" + saveID + ".wnmsave";
        bool exists = File.Exists(path);
        if (exists)
        {
            StreamReader reader = new StreamReader(path);
            string read = reader.ReadToEnd();
            reader.Close();
            saveStatus status = saveStatus.Working;
            try
            {
                JsonUtility.FromJson<saveData>(read);
            }
            catch (System.ArgumentException e)
            {
                status = saveStatus.Corrupt;
            }
            return status;
        }
        else
        {
            return saveStatus.Nonexistant;
        }
    }
}