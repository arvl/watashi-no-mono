﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PromptUI : MonoBehaviour {
    public RectTransform prompt;
    public Canvas promptCanvas;
    public Image fill;
    public Image bg;
    public Text promptKey;
    public Text promptKeyShadow;
    public Text promptText;
    public Animator promptAnimator;
    public int promptNumber;
    public Prompt myPrompt;
    public bool isFilling;
}
