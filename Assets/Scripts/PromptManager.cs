﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PromptManager : MonoBehaviour {
    #region References
    public List<Prompt> allPrompts;
    [SerializeField] GameObject promptBase;
    [SerializeField] GameObject promptIndicatorBase;
    [SerializeField] Prompt currentPrompt;
    [SerializeField] List<PromptUI> PromptUIs;
    [SerializeField] Sprite[] promptSprites;
    [SerializeField] PlayerController player;
    public Prompt lastPrompt;
    #endregion

    // Find all the prompts in the scene
    void Start()
    {
        Prompt[] prompts = GameObject.FindObjectsOfType<Prompt>();
        foreach (Prompt prompt in prompts)
        {
            allPrompts.Add(prompt);
        }
    }

    // Update every desired frame
    void LateUpdate()
    {
        updatePrompts();
    }

    // Find a prompt closest to the center of the screen
    Prompt findClosestPrompt()
    {
        Prompt closestPrompt = null;

        // The range a prompt can be on the screen
        float closest = Screen.height / 2.5f;

        foreach (Prompt prompt in allPrompts)
        {
            if (prompt.gameObject.activeInHierarchy && prompt.active)
            {
                // Get the position of the object on the player's screen
                Vector3 promptScreenPosition = player.references.mainCamera.cam.WorldToScreenPoint(prompt.transform.position);
                promptScreenPosition.z = 0;

                // Find the center of the screen's position
                Vector3 centerOfScreen = new Vector3(Screen.width / 2, Screen.height / 2, 0);

                // Find the distance of the prompt to the center of the screen
                float distanceToCenterOfScreen = Vector3.Distance(centerOfScreen, promptScreenPosition);

                // Find the closest prompt from the center of the screen
                if (distanceToCenterOfScreen < closest)
                {
                    // Find the distance of the prompt from the player, and make sure it's able to be found from any height from the player
                    float promptY = prompt.transform.position.y;

                    // Lock the Y position to the player's min & max height so the player cant use prompts at a floor below or above
                    float minPromptHeight = player.transform.position.y;
                    float maxPromptHeight = player.references.Head.position.y + 0.15f;
                    Mathf.Clamp(promptY, minPromptHeight, maxPromptHeight);

                    // Calculate the accurate distance from the player
                    Vector3 playerPosition = new Vector3(player.transform.position.x, promptY, player.transform.position.z);
                    float accurateDistanceToPlayer = Vector3.Distance(playerPosition, prompt.transform.position);
                    bool promptIsNear = accurateDistanceToPlayer <= prompt.maxDistance;

                    // See if the prompt is on the same floor as the player
                    float minPlayerHeight = player.transform.position.y;
                    float maxPlayerHeight = player.transform.position.y + 2;
                    bool sameFloor = prompt.transform.position.y >= minPlayerHeight && prompt.transform.position.y < maxPlayerHeight;

                    if (promptIsNear && sameFloor)
                    {
                        // See if the prompt is behind a wall or not
                        bool behindWall = Physics.Linecast(player.references.mainCamera.transform.position, prompt.transform.position, player.references.worldLayers);
                        
                        if (!behindWall)
                        {
                            closest = distanceToCenterOfScreen;
                            closestPrompt = prompt;
                        }
                    }
                }
            }
        }

        return closestPrompt;
    }

    // Find the closest prompt and make it visible, and see if the player is activating it
    void updatePrompts()
    {
        if (player.stats.promptsEnabled)
        {
            // Scan the area for a prompt
            currentPrompt = findClosestPrompt();

            if (currentPrompt != null && currentPrompt != lastPrompt)
            {
                clearPrompts();
                for (int i = 0; i < currentPrompt.info.Length; i++)
                {
                    spawnPrompt(currentPrompt.info[i], i);
                }
            }

            // If there's a prompt, make it appear
            if (currentPrompt != null)
            {
                player.references.headLook.lookAtPoint = currentPrompt.transform;
                if (PromptUIs.Count == 0)
                {
                    for (int i = 0; i < currentPrompt.info.Length; i++)
                    {
                        spawnPrompt(currentPrompt.info[i], i);
                    }
                }
                else
                {
                    foreach (PromptUI prompt in PromptUIs)
                    {
                        if (prompt != null)
                        {
                            updatePrompt(prompt, currentPrompt.info[prompt.promptNumber]);
                        }
                    }
                }

                updatePromptControls();
            }
            else
            {
                if (player.stats.playerState != playerStatus.Talking)
                {
                    player.references.headLook.lookAtPoint = null;
                    clearPrompts();
                }
            }

            for (var i = PromptUIs.Count - 1; i > -1; i--)
            {
                if (PromptUIs[i] == null)
                    PromptUIs.RemoveAt(i);
            }

            lastPrompt = currentPrompt;
        }
        else
        {
            player.references.headLook.enabled = player.stats.playerState == playerStatus.Talking ? true : false;
            clearPrompts();
        }
    }

    // Spawn a prompt
    public void spawnPrompt(promptInfo character, int number)
    {
        if (character != null)
        {
            GameObject spawnedPrompt = Instantiate(promptBase);
            PromptUI myPrompt = spawnedPrompt.GetComponent<PromptUI>();

            if (myPrompt != null)
            {
                #region Set the prompt's key text
                if (character.promptKey == "Interact")
                {
                    if (player.stats.keyboard)
                        myPrompt.promptKey.text = "E";
                    else
                        myPrompt.promptKey.text = "╳";
                }
                else if (character.promptKey == "Attack")
                {
                    if (player.stats.keyboard)
                        myPrompt.promptKey.text = "F";
                    else
                        myPrompt.promptKey.text = "△";
                }
                else if (character.promptKey == "Pick up")
                {
                    if (player.stats.keyboard)
                        myPrompt.promptKey.text = "R";
                    else
                        myPrompt.promptKey.text = "□";
                }
                #endregion

                #region Set the prompt's sprite
                switch (character.renderType)
                {
                    case promptType.Normal:
                        myPrompt.fill.sprite = promptSprites[0];
                        myPrompt.bg.sprite = promptSprites[0];
                        break;
                    case promptType.Dangerous:
                        myPrompt.fill.sprite = promptSprites[1];
                        myPrompt.bg.sprite = promptSprites[1];
                        break;
                }
                #endregion

                if (lastPrompt != currentPrompt)
                {
                    myPrompt.fill.fillAmount = 0;
                }
                myPrompt.promptNumber = number;
                myPrompt.promptCanvas.enabled = true;

                PromptUIs.Add(myPrompt);
            }
        }
    }

    // Update a spawned prompt
    void updatePrompt(PromptUI myPrompt, promptInfo character)
    {
        if (myPrompt != null)
        {
            #region Set the prompt's key text
            if (character.promptKey == "Interact")
            {
                if (player.stats.keyboard)
                    myPrompt.promptKey.text = "E";
                else
                    myPrompt.promptKey.text = "╳";
            }
            else if (character.promptKey == "Attack")
            {
                if (player.stats.keyboard)
                    myPrompt.promptKey.text = "F";
                else
                    myPrompt.promptKey.text = "△";
            }
            else if (character.promptKey == "Pick up")
            {
                if (player.stats.keyboard)
                    myPrompt.promptKey.text = "R";
                else
                    myPrompt.promptKey.text = "□";
            }
            #endregion

            myPrompt.promptKeyShadow.text = myPrompt.promptKey.text;

            #region Set the prompt's sprite
            switch (character.renderType)
            {
                case promptType.Normal:
                    myPrompt.fill.sprite = promptSprites[0];
                    myPrompt.bg.sprite = promptSprites[0];
                    break;
                case promptType.Dangerous:
                    myPrompt.fill.sprite = promptSprites[1];
                    myPrompt.bg.sprite = promptSprites[1];
                    break;
            }
            #endregion

            myPrompt.promptText.text = character.promptText.ToUpper();
            myPrompt.prompt.position = player.references.mainCamera.cam.WorldToScreenPoint(currentPrompt.transform.position + character.promptOffset);
        }
    }

    // Remove all prompts
    public void clearPrompts()
    {
        foreach (PromptUI prompt in PromptUIs)
        {
            if (prompt != null)
            {
                Destroy(prompt.gameObject);
            }
        }
        PromptUIs.RemoveAll((o) => o == null);
    }

    // Controls for the current enabled prompt
    void updatePromptControls()
    {
        foreach (PromptUI selectedPrompt in PromptUIs)
        {
            if (selectedPrompt)
            {
                // Fill the prompt if the correct key is pressed
                if (Input.GetButtonDown(currentPrompt.info[selectedPrompt.promptNumber].promptKey))
                {
                    selectedPrompt.isFilling = true;
                    currentPrompt.info[selectedPrompt.promptNumber].OnFillStart.Invoke();
                }
                if (Input.GetButtonUp(currentPrompt.info[selectedPrompt.promptNumber].promptKey))
                {
                    selectedPrompt.isFilling = false;
                    currentPrompt.info[selectedPrompt.promptNumber].OnFillEnd.Invoke();
                }
                if (selectedPrompt.isFilling)
                {
                    selectedPrompt.fill.fillAmount += Time.deltaTime * currentPrompt.info[selectedPrompt.promptNumber].fillSpeed;
                }
                else
                {
                    selectedPrompt.fill.fillAmount -= Time.deltaTime * 6;
                }
                if (selectedPrompt.fill.fillAmount == 1f)
                {
                    selectedPrompt.fill.fillAmount = 0;
                    if (!currentPrompt.info[selectedPrompt.promptNumber].fillAfterFinished)
                    {
                        selectedPrompt.isFilling = false;
                    }
                    currentPrompt.info[selectedPrompt.promptNumber].OnFill.Invoke();
                    if (Input.GetButton(currentPrompt.info[selectedPrompt.promptNumber].promptKey))
                        currentPrompt.info[selectedPrompt.promptNumber].OnFillStart.Invoke();
                }
            }
        }
    }
}
