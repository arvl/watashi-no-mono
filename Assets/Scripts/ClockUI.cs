﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class ClockUI : MonoBehaviour
{
    [Header("Time")]
    public time currentTime;
    public WNM_Event[] dayEvents;
    public WNM_Event currentEvent;
    public float realtimeSecondsToIngameMinutes = 2f;
    [Header("UI")]
    public Text timeUI;
    public Text eventUI;
    [Header("References")]
    public Students studentManager;
    public UnityEvent TimeChanged = new UnityEvent();

    // Start the clock
    private void Start ()
    {
        StartCoroutine(clockTick());
        SoundManager.Play("Schoolbell");
    }
	
    // Main time script
	private IEnumerator clockTick() {
        while (true)
        {
            yield return new WaitForSeconds(realtimeSecondsToIngameMinutes);
            currentTime.minute += 1;
            if (currentTime.minute == 60)
            {
                currentTime.minute = 0;
                currentTime.hour += 1;
            }
            if (currentTime.hour > 12)
            {
                currentTime.hour = 1;
                currentTime.PM = !currentTime.PM;
            }
            TimeChanged.Invoke();
            checkEvents();
            updateUI();
        }
    }

    // Check all the schoolday events to see if one has arrived
    void checkEvents()
    {
        foreach (WNM_Event dayEvent in dayEvents)
        {
            if (currentTime.hour == dayEvent.hour && currentTime.minute == dayEvent.minute)
            {
                alertStudentsEventChanged();
                currentEvent = dayEvent;
                SoundManager.Play("Schoolbell");
                break;
            }
        }
    }

    void alertStudentsEventChanged()
    {
        foreach (Student student in studentManager.allStudents)
        {
            student.onTimeEventChanged();
        }
    }

    // Set the clock UI to the correct text
    private void updateUI() {
        timeUI.text = currentTime.hour.ToString() + (currentTime.minute < 10 ? ":0" : ":") + currentTime.minute.ToString() + (currentTime.PM ? " PM" : " AM");
        eventUI.text = currentEvent.eventName;
    }
}