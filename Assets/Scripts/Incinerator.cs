﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Incinerator : MonoBehaviour {
    public PlayerController player;
    public bool open;
    public Animator door;
    public Prompt myPrompt;
    public AudioSource audioSRC;
    public AudioClip[] doorSounds;
    public Transform[] dropPositions;
    public Animator gasDoorAnimator;
    public Prompt gasDoorPrompt;
    public ParticleSystem[] incineratorParticles;
    public ParticleSystem openDoorParticles;
    public Prompt activatePrompt;
    public List<GameObject> itemsInside;

    public bool fueled;
    public bool activated;

    void Update()
    {
        if (player.stats.playerState != playerStatus.Dragging)
        {
            myPrompt.active = true;
            if (player.stats.itemEquipped == 0)
            {
                myPrompt.info[0].promptText = open ? "Close" : "Open";
            }
            else
            {
                myPrompt.info[0].promptText = "Dispose";
            }
        }
        else
        {
            myPrompt.active = false;
        }
        if (fueled)
        {
            activatePrompt.info[0].promptText = "Activate";
        }
        else
        {
            activatePrompt.info[0].promptText = "Not fueled";
        }
    }

    IEnumerator destroyObject(float delay, GameObject objectToDestroy)
    {
        yield return new WaitForSeconds(delay);
        itemsInside.Add(objectToDestroy);
        objectToDestroy.SetActive(false);
    }

    public void interact()
    {
        if (player.stats.itemEquipped == 0)
        {
            audioSRC.PlayOneShot(open ? doorSounds[0] : doorSounds[1]);
            door.Play(open ? "Incinerator_Close" : "Incinerator_Open");
            open = !open;
        }
        else
        {
            if (player.references.inventoryManager.selected.myItem != null)
            {
                if (!open)
                {
                    audioSRC.PlayOneShot(open ? doorSounds[0] : doorSounds[1]);
                    door.Play(open ? "Incinerator_Close" : "Incinerator_Open");
                    open = !open;
                }
                Item itemDropped = player.references.inventoryManager.selected.myItem;
                player.references.inventoryManager.dropItem(player.references.inventoryManager.current);
                itemDropped.myPrompt.active = false;
                itemDropped.transform.position = dropPositions[0].position;
                StartCoroutine(destroyObject(1f, itemDropped.gameObject));
            }
        }
    }

    public void fuelIncinerator()
    {
        if (!fueled)
        {
            fueled = true;
            gasDoorAnimator.Play("Open");
            gasDoorPrompt.gameObject.SetActive(false);
        }
    }

    public void activateIncinerator()
    {
        if (fueled && !activated)
        {
            foreach (ParticleSystem fx in incineratorParticles)
            {
                fx.Play();
            }
            audioSRC.PlayOneShot(doorSounds[2]);
            myPrompt.gameObject.SetActive(false);
            foreach (GameObject item in itemsInside)
            {
                item.name = "DestroyedObject";
            }
            if (open)
                openDoorParticles.Play();
        }
    }
}
