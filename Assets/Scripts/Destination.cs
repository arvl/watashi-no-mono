﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destination : MonoBehaviour
{
    public DestinationAction actionOnArrival;
    public List<Destination> requiredArrivals;
}