﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Door : MonoBehaviour {
    public doorType OpenDoorType;
    public bool open;
    public Transform[] doors;
    public Collider doorCollider;
    public Prompt myPrompt;
    public AudioSource audioSRC;
    public AudioClip[] doorSounds;

	void Update () {
        myPrompt.info[0].promptText = open ? "Close" : "Open";
        doorCollider.enabled = !open;
        if (open) updateOpenDoor(); else updateClosedDoor();
	}

    public void toggleDoor()
    {
        open = !open;
        if (doorSounds.Length == 0)
        {
            audioSRC.PlayOneShot(audioSRC.clip);
        }
        else
        {
            audioSRC.PlayOneShot(open ? doorSounds[0] : doorSounds[1]);
        }
    }

    void updateOpenDoor()
    {
        switch (OpenDoorType)
        {
            case doorType.SwingingDoubleDoor:
                doors[0].transform.localEulerAngles = Vector3.Lerp(doors[0].transform.localEulerAngles, new Vector3(0, 135, 0), Time.deltaTime * 8);
                doors[1].transform.localEulerAngles = Vector3.Lerp(doors[1].transform.localEulerAngles, new Vector3(0, 45, 0), Time.deltaTime * 8);
                break;
            case doorType.SingleSwingingDoor:
                doors[0].transform.localEulerAngles = Vector3.Lerp(doors[0].transform.localEulerAngles, new Vector3(0, 135, 0), Time.deltaTime * 8);
                break;
            case doorType.SlidingDoubleDoor:
                doors[0].transform.localPosition = Vector3.Lerp(doors[0].transform.localPosition, new Vector3(-0.444f, 0, 0.009333285f), Time.deltaTime * 8);
                break;
            case doorType.BathroomStallDoor:
                doors[0].transform.localEulerAngles = Vector3.Lerp(doors[0].transform.localEulerAngles, new Vector3(0, 90, 0), Time.deltaTime * 8);
                break;
        }
    }

    void updateClosedDoor()
    {
        switch (OpenDoorType)
        {
            case doorType.SwingingDoubleDoor:
                doors[0].transform.localEulerAngles = Vector3.Lerp(doors[0].transform.localEulerAngles, new Vector3(0, 0, 0), Time.deltaTime * 8);
                doors[1].transform.localEulerAngles = Vector3.Lerp(doors[1].transform.localEulerAngles, new Vector3(0, 180, 0), Time.deltaTime * 8);
                break;
            case doorType.SingleSwingingDoor:
                doors[0].transform.localEulerAngles = Vector3.Lerp(doors[0].transform.localEulerAngles, new Vector3(0, 0, 0), Time.deltaTime * 8);
                break;
            case doorType.SlidingDoubleDoor:
                doors[0].transform.localPosition = Vector3.Lerp(doors[0].transform.localPosition, new Vector3(0.4767358f, 0, 0.009333285f), Time.deltaTime * 8);
                break;
            case doorType.BathroomStallDoor:
                doors[0].transform.localEulerAngles = Vector3.Lerp(doors[0].transform.localEulerAngles, new Vector3(0, 0, 0), Time.deltaTime * 8);
                break;
        }
    }
}