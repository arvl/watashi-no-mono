﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DumpsterLid : MonoBehaviour {
    public bool open;
    public Transform door;
    public Prompt myPrompt;
    PlayerController player;

    void Start()
    {
        player = GameObject.FindObjectOfType<PlayerController>();
    }

    void Update()
    {
        updateDoorRotation();
        updatePrompt();
    }

    public void activate()
    {
        if (player.stats.itemEquipped == 0)
        {
            open = !open;
            SoundManager.Play(open ? "IncineratorOpen" : "IncineratorClose");
        }
        else
        {
            if (player.references.inventoryManager.selected.myItem != null && open)
            {
                Item itemDropped = player.references.inventoryManager.selected.myItem;
                player.references.inventoryManager.dropItem(player.references.inventoryManager.current);
                itemDropped.myPrompt.active = false;
                itemDropped.transform.position = myPrompt.transform.position + new Vector3(0, 0.2f, 0);
                StartCoroutine(destroyObject(1f, itemDropped.gameObject));
            }
            else
            {
                open = !open;
                SoundManager.Play(open ? "IncineratorOpen" : "IncineratorClose");
            }
        }
    }

    IEnumerator destroyObject(float delay, GameObject objectToDestroy)
    {
        yield return new WaitForSeconds(delay);
        objectToDestroy.SetActive(false);
    }

    void updatePrompt()
    {
        if (player.stats.itemEquipped == 0)
        {
            myPrompt.info[0].promptText = open ? "Close" : "Open";
        }
        else
        {
            myPrompt.info[0].promptText = open ? "Toss" : "Open";
        }
    }

    void updateDoorRotation()
    {
        if (open)
        {
            door.transform.localRotation = Quaternion.Lerp(door.transform.localRotation, Quaternion.Euler(new Vector3(-107, 0, 0)), Time.deltaTime * 8);
        }
        else
        {
            door.transform.localRotation = Quaternion.Lerp(door.transform.localRotation, Quaternion.Euler(new Vector3(0, 0, 0)), Time.deltaTime * 20);
        }
    }
}
