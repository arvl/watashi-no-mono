﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {
    [SerializeField] PlayerController player;
    public schooldayTrack[] schooldayTracks;
    public AudioSource saneMusic;
    public AudioSource insaneMusic;
    public bool isMusicPlaying;

    void Start () {
        randomizeMusic();
    }

    public void randomizeMusic()
    {
        schooldayTrack randomMusic = schooldayTracks[Random.Range(0, schooldayTracks.Length)];
        setBackgroundTrack(randomMusic.saneAudio, randomMusic.insaneAudio);
    }

    public static void setBackgroundTrack(AudioClip saneTrack, AudioClip insaneTrack)
    {
        MusicManager musicManager = GameObject.FindObjectOfType<MusicManager>();
        musicManager.saneMusic.clip = saneTrack;
        musicManager.insaneMusic.clip = insaneTrack;
    }

    public static void stopMusic()
    {
        MusicManager musicManager = GameObject.FindObjectOfType<MusicManager>();
        musicManager.saneMusic.Stop();
        musicManager.insaneMusic.Stop();
        musicManager.isMusicPlaying = false;
    }

    public static void playMusic()
    {
        MusicManager musicManager = GameObject.FindObjectOfType<MusicManager>();
        musicManager.saneMusic.Play();
        musicManager.insaneMusic.Play();
        musicManager.isMusicPlaying = true;
    }

    public static bool musicPlaying()
    {
        MusicManager musicManager = GameObject.FindObjectOfType<MusicManager>();
        return musicManager.isMusicPlaying;
    }

    void Update()
    {
        if (player != null)
        {
            if (player.stats.canMovePlayer())
            {
                float insanity = 1 - (player.stats.sanity / 4);
                saneMusic.volume = Mathf.Lerp(saneMusic.volume, player.stats.sanity / 4 * PlayerPrefs.GetFloat("musicVolume"), Time.deltaTime * 4);
                insaneMusic.volume = Mathf.Lerp(insaneMusic.volume, insanity * PlayerPrefs.GetFloat("musicVolume"), Time.deltaTime * 4);
            }
        }
    }
}
