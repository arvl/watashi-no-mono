﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine.PostProcessing;
using Kino;

public class PlayerController : MonoBehaviour {
    #region Variables
    [Header("Player Controller")]
    public movementInfo speed;
    public playerInfo stats;
    public playerReferences references;
    public movementAnimations moveAnims;
    public bool showUI = true;
    public PostProcessingBehaviour ImageEffect;
    public DigitalGlitch glitch1;
    public AnalogGlitch glitch2;
    public Transform spotToLerpTo;
    public int studentsSeePlayer;
    public List<IDCardInfo> stolenCards = new List<IDCardInfo>();
    public bool canChangeClothing;
    Student[] allStudents;
    #endregion

    // Get all the prompts in the scene
    void Start() {
        PlayerPrefs.SetInt("SaveFileID", 0);
        allStudents = GameObject.FindObjectsOfType<Student>();
        Application.targetFrameRate = 60;
        stats.currentAnimation = moveAnims.idleAnim;
        resetImageEffects();
        SaveManager.loadGame(PlayerPrefs.GetInt("SaveFileID"));

        // Start cutscene
        StartCoroutine(schooldayStartCutscene());
    }

    IEnumerator schooldayStartCutscene()
    {
        references.fadeManager.alphaGroup.alpha = 1;
        references.fadeManager.fadeSpeed = 2;

        showUI = false;
        Vector3 wantedPos = transform.position + references.mainCamera.targetOffset - new Vector3(0, 0.025f, 0) + (transform.right * references.mainCamera.rightOffset) - (transform.forward * references.mainCamera.distance);
        stats.canMove = playerMovementMode.none;
        references.mainCamera.transform.position = wantedPos + new Vector3(0, 5, 0);

        yield return new WaitForSeconds(0.5f);

        references.fadeManager.fade = false;

        yield return new WaitForSeconds(1f);

        bool skipped = false;
        while (references.mainCamera.transform.position.y > wantedPos.y + 0.025f)
        {
            references.mainCamera.transform.position = Vector3.Lerp(references.mainCamera.transform.position, wantedPos, Time.fixedDeltaTime);
            if (Input.GetButton("Skip Cutscene") || Input.GetButton("Interact"))
            {
                skipped = true;
                break;
            }
            yield return new WaitForFixedUpdate();
        }

        showUI = true;

        if (!skipped) yield return new WaitForSeconds(1f);

        MusicManager.playMusic();
        stats.canMove = playerMovementMode.all;
    }

    // Reset the image effects
    void resetImageEffects()
    {
        /*ColorGradingModel.Settings fx = ImageEffect.profile.colorGrading.settings;
        fx.basic.saturation = 1f;
        fx.basic.contrast = 1.25f;
        ImageEffect.profile.colorGrading.settings = fx;*/
    }

    // Run every frame
    void Update() {
        LimitedUpdate();

        // Controls
        updateCamera();
        updateCameraFOV();
        updateShoveAnimationLayer();
        toggleCrouch();
        updateDrag();
        updateUIAlpha();
        updateItem();

        // UI
        updateTalking();
    }

    // Run every physics update
    void FixedUpdate()
    {
        updateMovement();
    }

    // Prevent the player from leaving the school
    IEnumerator cancelLeaveSchool()
    {
        if (stats.playerState == playerStatus.Nothing && !leavingSchool)
        {
            leavingSchool = true;

            // Move the camera, freeze the player, make them turn around, and add a subtitle
            SubtitlesUI.Get().AddText("I can't leave during school!", 5f);
            stats.canMove = playerMovementMode.none;
            references.mainCamera.transform.position = new Vector3(transform.position.x, 5f, -45f);
            references.mainCamera.transform.eulerAngles = new Vector3(0f, 0f, 0f);
            transform.eulerAngles = new Vector3(0, 180, 0);
            playAnimation("turnAround", 0.1f);
            yield return new WaitForSeconds(2.25f);
            // Reposition the player
            playAnimation(moveAnims.idleAnim, 0f);
            transform.position = new Vector3(transform.position.x, 3f, -39f);
            transform.eulerAngles = new Vector3(0, 0, 0);
            stats.canMove = playerMovementMode.all;

            leavingSchool = false;
        }
    }

    // Check if the player is trying to leave school
    bool leavingSchool = false;
    void checkPlayerLeavingSchool()
    {
        if (transform.position.z < -39.3f)
        {
            StartCoroutine(cancelLeaveSchool());
        }
    }

    // Fire when the player enters a trigger collider
    void OnTriggerEnter(Collider other)
    {
        switch (other.name)
        {
            case "breakableTree":
                Student creepyStudent = GameObject.Find("Gina Fumiko").GetComponent<Student>();

                if (creepyStudent && creepyStudent.character.status == characterStatus.Normal && stats.playerState == playerStatus.Nothing)
                {
                    if (Operators.Distance(creepyStudent.transform.position, other.transform.position) < 3)
                    {
                        SoundManager.Play("Interrogate_DeskSlam");
                        other.transform.eulerAngles = new Vector3(180, 0, 0);
                        creepyStudent.character.status = characterStatus.Scared;
                        creepyStudent.character.playFaceAnimation("Face_Scared", 0.25f);
                        creepyStudent.character.pauseMovement();
                        StartCoroutine(creepyStudent.reactToBody(creepyStudent));
                    }
                }
                break;
            case "shoveCollider":
                if (stats.currentAnimation == moveAnims.runAnim)
                {
                    Student student = other.transform.parent.GetComponent<Student>();

                    bool canBeShoved = 
                        student.character.status != characterStatus.BeingShoved && 
                        (student.character.status == characterStatus.Normal || student.character.status == characterStatus.Suspicious);

                    if (canBeShoved && !student.character.ragdoll.ragdolled)
                    {
                        if (student.timesShoved < 5)
                        {
                            StartCoroutine(student.shove(transform));
                            StartCoroutine(shoveAnimation(student.transform));
                        }
                    }
                }
                break;
        }
    }

    // Play the player's shove animation if the player shoves a student
    IEnumerator shoveAnimation(Transform origin)
    {
        shoveAnimationLayerEnabled = true;

        Vector3 cross = Vector3.Cross(transform.forward, origin.position);
        float dir = Vector3.Dot(cross, transform.up);

        // If dir is over -0.5, play shove_Right, else, if dir is less than 0.5, play shove left. Else, play shove_Right.
        references.animator.CrossFadeInFixedTime("None", 0.01f);
        references.animator.CrossFadeInFixedTime(dir > -0.5f ? "shove_Right" : dir < 0.5f ? "shove_Left" : "shove_Right", 0.1f);
        SoundManager.Play("shove_" + Random.Range(0, 2), PlayerPrefs.GetFloat("effectVolume"));

        yield return new WaitForSeconds(0.8f);
        shoveAnimationLayerEnabled = false;
    }

    // Update the shove layer in the Animator
    bool shoveAnimationLayerEnabled;
    void updateShoveAnimationLayer()
    {
        float current = references.animator.GetLayerWeight(2);
        references.animator.SetLayerWeight(2, Mathf.Lerp(current, shoveAnimationLayerEnabled ? 0.75f : 0, Time.deltaTime * 15));
    }

    // Update every desired frame
    int currentFrame;
    void LimitedUpdate()
    {
        if (currentFrame == 5)
        {
            currentFrame = 0;

            updateSanity();
            updateBloodiness();
            updateSanity();
            checkPlayerLeavingSchool();

            if (canChangeClothing) updateClothing(); 
        }
        else
        {
            currentFrame++;
        }
    }

    // Update the player's clothing mesh
    void updateClothing()
    {
        references.skirtMesh.enabled = stats.currentClothing == clothingType.Default ? true : false;
        switch (stats.currentClothing)
        {
            case clothingType.Default:
                references.clothingMesh.sharedMesh = references.clothingMeshes[0];
                references.clothingMesh.materials = references.defaultClothingMaterials;
                break;
            case clothingType.Gym:
                references.clothingMesh.sharedMesh = references.clothingMeshes[1];
                references.clothingMesh.materials = references.gymClothingMaterials;
                break;
        }
    }

    // Update the UI's alpha
    void updateUIAlpha()
    {
        if (references.mainUICanvas != null)
        {
            references.mainUICanvas.alpha = Mathf.Lerp(references.mainUICanvas.alpha, showUI ? 1.1f : -0.1f, Time.deltaTime * 5);
        }
    }

    // Change the sanity effects & more depending on the player sanity level
    public void updateSanity()
    {
        float insanity = 1 - (stats.sanity / 4);
        references.faceAnimator.SetLayerWeight(1, insanity);

        /*glitch1.intensity = insanity * 0.01f;

        ColorGradingModel.Settings fx = ImageEffect.profile.colorGrading.settings;
        fx.basic.saturation = 1f - (insanity * 0.5f);
        fx.basic.contrast = 1.25f + (insanity * 0.5f);
        ImageEffect.profile.colorGrading.settings = fx;

        glitch2.enabled = insanity > 0f;
        glitch2.horizontalShake = insanity * 0.01f;
        glitch2.colorDrift = insanity * 0.01f;
        glitch2.scanLineJitter = insanity * 0.01f;
        glitch2.verticalJump = insanity * 0.001f;*/

        if (stats.playerState == playerStatus.Sitting)
            stats.sanity += 0.1f * Time.deltaTime;

        if (stats.sanity < 0)
            stats.sanity = 0;

        if (stats.sanity > 4)
            stats.sanity = 4;
    }

    // Change the bloodiness texture to the correct one depending on the player's bloodiness level
    void updateBloodiness()
    {
        references.bloodProjector.material = references.bloodMaterials[stats.bloodiness];
    }

    // Update the current student being dragged's limb position and rotation
    void updateDrag()
    {
        if (stats.playerState == playerStatus.Dragging)
        {
            stats.selectedStudent.dragLimb.transform.position = Vector3.Lerp(stats.selectedStudent.dragLimb.transform.position, references.dragPosition.position, Time.deltaTime * 25);
            stats.selectedStudent.dragLimb.transform.rotation = Quaternion.Lerp(stats.selectedStudent.dragLimb.transform.rotation, references.dragPosition.rotation, Time.deltaTime * 25);
        }
    }

    // See what animation to play with the current item in-hand
    void updateItem() {
        bool animateItem = false;
        if (stats.itemEquipped == 1)
        {
            // Knife
            animateItem = true;
            playItemAnimation("knifeHold_START", 0.05f);
        }
        float newWeight = 0;
        if (animateItem && stats.playerState != playerStatus.Attacking)
        {
            newWeight = Mathf.Lerp(references.animator.GetLayerWeight(1), 1f, Time.deltaTime * 8);
        }
        else
        {
            newWeight = Mathf.Lerp(references.animator.GetLayerWeight(1), 0f, Time.deltaTime * 8);
            stats.currentItemAnimation = "";
        }
        references.animator.SetLayerWeight(1, newWeight);
    }

    void updateCameraFOV()
    {
        if (stats.selectedStudent != null && (stats.playerState == playerStatus.Talking || stats.playerState == playerStatus.Attacking))
        {
            bool lerpFOV = false;

            if (stats.playerState == playerStatus.Talking)
            {
                Vector3 positionToLook = references.Head.position + (references.Head.forward * (references.Head.position - stats.selectedStudent.Head.position).magnitude / 2);
                references.mainCamera.transform.rotation = Quaternion.Lerp(references.mainCamera.transform.rotation, Quaternion.LookRotation(positionToLook - references.mainCamera.transform.position), Time.deltaTime * 3f);
                lerpFOV = true;
            }
            else if (stats.playerState == playerStatus.Attacking)
            {
                lerpFOV = true;
            }

            if (lerpFOV)
            {
                references.mainCamera.cam.fieldOfView = Mathf.Lerp(references.mainCamera.cam.fieldOfView, 30f, Time.deltaTime * 4);
            }
        }
        else
        {
            references.mainCamera.cam.fieldOfView = Mathf.Lerp(references.mainCamera.cam.fieldOfView, 60, Time.deltaTime * 4);
            if (!stats.canMovePlayer() && stats.selectedStudent != null)
            {
                Vector3 lookDir = (transform.position + references.mainCamera.targetOffset) - references.mainCamera.transform.position + (transform.right * references.mainCamera.rightOffset);
                references.mainCamera.transform.rotation = Quaternion.Lerp(references.mainCamera.transform.rotation, Quaternion.LookRotation(lookDir), Time.deltaTime * 4f);
            }
        }
    }

    bool studentSitting;
    bool ableToStop;
    void updateTalking() {
        if ((stats.playerState == playerStatus.Talking || stats.playerState == playerStatus.Attacking) && stats.selectedStudent != null)
        {
            if (stats.playerState == playerStatus.Talking)
            {
                showUI = false;
                stats.canMove = playerMovementMode.none;

                if (stats.currentAnimation != moveAnims.sitAnim)
                {
                    Vector3 wantedDir = stats.selectedStudent.transform.position - transform.position;
                    wantedDir.y = 0;
                    transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(wantedDir), Time.deltaTime * 4);
                }
                else
                {
                    references.headLook.lookAtPoint = stats.selectedStudent.Head;
                }
            }
        }
        else
        {
            if (!stats.canMovePlayer())
            {
                if (stats.selectedStudent != null)
                {
                    if (stats.currentAnimation != moveAnims.sitAnim)
                    {
                        studentSitting = false;

                        // Fix the student
                        stats.selectedStudent.character.resumeMovement();
                        stats.selectedStudent.character.setDestination(stats.selectedStudent.character.currentDestination, stats.selectedStudent.character.running);
                        stats.selectedStudent.character.lookAt(null);
                    }
                    else
                    {
                        studentSitting = true;

                        // Fix the student
                        stats.selectedStudent.character.lookAt(null);
                    }

                    // Clear the selected student variable
                    stats.selectedStudent = null;
                    ableToStop = true;
                }
                if (ableToStop && references.mainCamera.cam.fieldOfView - 60 > -0.2f && stats.playerState == playerStatus.Nothing)
                {
                    ableToStop = false;
                    if (!studentSitting)
                    {
                        stats.canMove = playerMovementMode.all;
                    }
                    else
                    {
                        stats.canMove = playerMovementMode.cameraOnly;
                    }
                }
            }
        }
    }

    // Disable talking
    public void stopTalking()
    {
        // Disable talking
        showUI = true;
        stats.playerState = playerStatus.Nothing;
        stats.selectedStudent.character.status = characterStatus.Normal;
        stats.selectedStudent.character.lookAt(null);
        stats.selectedStudent.character.spotAnimated = false;
        stats.selectedStudent.HeadLook.lookAtPoint = null;
        references.talkManager.deactivate();

        // Fix the player
        if (stats.currentAnimation != moveAnims.sitAnim) playAnimation(moveAnims.idleAnim, 0.2f);
        stats.promptsEnabled = true;
    }

    // See if the player is using a keyboard or a controller
    void checkKeyboardOrController() {
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D))
        {
            stats.keyboard = true;
        }
        else
        {
            stats.keyboard = false;
        }
    }

    // Update the player's movement
    Vector3 moveDirection;
    Quaternion wantedRotation;
    void updateMovement() {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        float wantedSpeed = 0;

        if (stats.playerState != playerStatus.Attacking)
        {
            references.animator.speed = 1;
        }

        // Lerp the player to spotToLerpTo if it's assigned
        if (spotToLerpTo != null)
        {
            transform.position = Vector3.Lerp(transform.position, spotToLerpTo.position, Time.deltaTime * 8);
            transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, spotToLerpTo.eulerAngles, Time.deltaTime * 8);
        }

        // Determine if the player should move or not
        Vector3 moveInput = new Vector3(horizontal, 0, vertical);

        #region Player movement speed and animation
        if (moveInput.magnitude > 0)
        {
            checkKeyboardOrController();
            if (stats.canMovePlayer())
            {
                if (stats.playerState != playerStatus.Crouching)
                {
                    speed.turnSpeed = 8;
                    wantedSpeed = updateRegularMovement(wantedSpeed);
                }
                else
                {
                    speed.turnSpeed = 3;
                    wantedSpeed = updateCrouchedMovement(wantedSpeed);
                }
            }
        }
        else
        {
            if (stats.canMovePlayer())
            {
                if (stats.playerState != playerStatus.Crouching)
                {
                    playAnimation(moveAnims.idleAnim, 0.3f);
                }
                else
                {
                    playAnimation(moveAnims.crouchAnim, 0.3f);
                }
            }
        }
        #endregion

        moveDirection = transform.forward * wantedSpeed * Time.deltaTime;
        references.rigidbody.velocity = new Vector3(0, -15, 0);

        Vector3 currentPosition = references.rigidbody.position;

        #region Player movement rotation
        if (moveInput.magnitude > 0 && stats.canMovePlayer())
        {
            Vector3 wantedEuler = new Vector3(0, 0, 0);
            wantedEuler += references.mainCamera.transform.forward * vertical;
            wantedEuler += references.mainCamera.transform.right * horizontal;
            wantedRotation = Quaternion.LookRotation(wantedEuler);
            wantedRotation.x = 0;
            wantedRotation.z = 0;
            references.animator.SetFloat("turnAmount", wantedRotation.y - transform.rotation.y);
            references.rigidbody.rotation = Quaternion.Lerp(references.rigidbody.rotation, wantedRotation, Time.deltaTime * speed.turnSpeed);
            references.rigidbody.MovePosition(Vector3.Lerp(currentPosition, currentPosition + moveDirection / Time.deltaTime, Time.deltaTime));
        }
        #endregion
    }

    // Toggle crouch
    void toggleCrouch()
    {
        if (Input.GetButtonDown("Crouch"))
        {
            if (stats.playerState == playerStatus.Nothing)
            {
                stats.playerState = playerStatus.Crouching;
            }
            else if (stats.playerState == playerStatus.Crouching)
            {
                stats.playerState = playerStatus.Nothing;
            }
        }
    }
    
    // Update regular movement animations like walking & sprinting
    float updateRegularMovement(float wantedSpeed0)
    {
        if (!Input.GetButton("Sprint"))
        {
            playAnimation(moveAnims.walkAnim, 0.3f);
            return speed.walkSpeed;
        }
        else
        {
            if (stats.playerState != playerStatus.Dragging)
            {
                playAnimation(moveAnims.runAnim, 0.2f);
                return speed.sprintSpeed;
            }

            playAnimation(moveAnims.walkAnim, 0.3f);
            return speed.walkSpeed;
        }
    }

    // Update crouched movement animations like crawling
    float updateCrouchedMovement(float wantedSpeed0)
    {
        playAnimation(moveAnims.crawlAnim, 0.3f);
        return speed.crawlSpeed;
    }

    // Call the updateCamera() void from the camera
    void updateCamera() {
        if (stats.canMoveCamera())
            references.mainCamera.updateCamera();
    }

    // Play a random footstep sound
    void step() {
        SoundManager.PlayRandom(new string[5] {
            "ConcreteStep1",
            "ConcreteStep2",
            "ConcreteStep3",
            "ConcreteStep4",
            "ConcreteStep5"
        }, PlayerPrefs.GetFloat("effectVolume"));
    }

    // Drag a student
    public void drag(Student student) {
        if (stats.playerState != playerStatus.Dragging)
        {
            stats.selectedStudent = student;
            stats.playerState = playerStatus.Dragging;

            // Set player animations
            moveAnims.idleAnim = "dragBody_Idle";
            moveAnims.walkAnim = "dragBody_Walk";
            playAnimation(moveAnims.idleAnim, 0.25f);

            // Set player speed
            speed.walkSpeed = 1 - student.character.Weight;
            speed.turnSpeed = 3;

            // Freeze some limbs on the student
            stats.selectedStudent.dragArms[0].isKinematic = true;
            stats.selectedStudent.dragArms[1].isKinematic = true;
            stats.selectedStudent.Head.GetComponent<Rigidbody>().isKinematic = true;
        }
        else
        {
            stats.playerState = playerStatus.Nothing;

            // Set player animations
            moveAnims.idleAnim = "defaultIdle";
            moveAnims.walkAnim = "defaultWalk";
            playAnimation(moveAnims.idleAnim, 0.25f);

            // Reset player speed
            speed.walkSpeed = 1;
            speed.turnSpeed = 8;

            // Thaw student limbs
            stats.selectedStudent.dragArms[0].isKinematic = false;
            stats.selectedStudent.dragArms[1].isKinematic = false;
            stats.selectedStudent.dragLimb.isKinematic = false;
            stats.selectedStudent.Head.GetComponent<Rigidbody>().isKinematic = false;

            stats.selectedStudent = null;
        }
    }

    // Cross fade into an animation if the animation isn't playing
    public void playAnimation(string clipName, float transitionTime) {
        if (stats.currentAnimation != clipName) {
            stats.currentAnimation = clipName;
            references.animator.CrossFadeInFixedTime(clipName, transitionTime * references.animator.speed);
        }
    }
    void playItemAnimation(string clipName, float transitionTime)
    {
        if (stats.currentItemAnimation != clipName)
        {
            stats.currentItemAnimation = clipName;
            references.animator.CrossFadeInFixedTime(clipName, transitionTime);
        }
    }

    // Spawn a uniform
    public void spawnUniform(bool bloody)
    {
        GameObject uniform = Instantiate(references.uniformPrefabs[0]);
        uniform.transform.parent = null;
        uniform.transform.position = references.Head.position;
        uniform.transform.rotation = transform.rotation;
        references.promptManager.allPrompts.Add(uniform.transform.Find("Prompt").GetComponent<Prompt>());
        uniform.transform.Find("Mesh").GetComponent<MeshFilter>().mesh = references.clothingMesh.sharedMesh;
        switch (stats.currentClothing)
        {
            case clothingType.Default:
                uniform.transform.Find("Mesh").GetComponent<MeshRenderer>().materials = references.playerLocker.uniformMaterials;
                break;
            case clothingType.Gym:
                uniform.transform.Find("Mesh").GetComponent<MeshRenderer>().materials = references.playerLocker.gymMaterials;
                break;
        }

        references.policeManager.bloodyUniforms.Add(uniform.GetComponent<Item>());

        if (bloody)
        {
            stats.bloodiness = 0;
        }
    }

    public playerSuspicionTypes isSuspicious(Student me)
    {
        playerSuspicionTypes suspicionType = new playerSuspicionTypes();
        // See if the player is looking away from the student
        float angle = Vector3.Angle(transform.forward, me.transform.position - transform.position);

        if (stats.bloodiness > 0)
        {
            suspicionType = playerSuspicionTypes.bloody;
        }
        else if (stats.sanity < 3.5f)
        {
            suspicionType = playerSuspicionTypes.insane;
        }
        else if (stats.holdingWeapon() && angle > 80)
        {
            suspicionType = playerSuspicionTypes.hasSuspiciousItem;
        }
        else
        {
            suspicionType = playerSuspicionTypes.normal;
        }

        return suspicionType;
    }
}