﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PoliceManager : MonoBehaviour {
    #region References
    [SerializeField] PlayerController player;
    [SerializeField] MusicManager musicManager;
    [SerializeField] Image timerRoot;
    [SerializeField] Text timerText;
    [SerializeField] AudioClip[] policeMusic;
    public bool policeCalled;
    bool lastCalled;

    public GameObject interrogationRoom;
    public Transform playerInterrogationRoomSitPosition;
    public Transform interrogrationCameraPosition;
    public Student interrogatorOpinion;
    public Animator interrogatorAnimator;
    public Text policeSubtitle;
    bool policeSubtitleEnabled;
    public MeshRenderer policeGun;

    public List<Item> weaponsUsed;
    public List<Student> studentsKilled;
    public List<Item> bloodyUniforms;
    public int murderWitnesses;

    public GameObject policeTapeItemPrefab;
    public GameObject policeTapeBodyPrefab;
    public policeEvidenceItem[] savedEvidence = new policeEvidenceItem[0];

    int minute = 2;
    int second = 0;
    int currentPoliceState;
    #endregion

    void Start()
    {
        spawnSceneEvidence();
    }

    void spawnSceneEvidence()
    {
        savedEvidence = SceneController.loadSceneEvidence();

        if (savedEvidence != null)
        {
            foreach (policeEvidenceItem evidenceItem in savedEvidence)
            {
                if (evidenceItem.type == evidenceType.item)
                {
                    GameObject tapeClone = Instantiate(policeTapeItemPrefab);
                    tapeClone.transform.position = evidenceItem.position;
                    tapeClone.transform.eulerAngles = new Vector3(0, Random.Range(-360, 360), 0);

                    GameObject item = GameObject.Find(evidenceItem.name);
                    item.transform.position = evidenceItem.position;
                    item.transform.Find("Prompt").gameObject.SetActive(false);
                }
                else if (evidenceItem.type == evidenceType.body)
                {
                    GameObject tapeClone = Instantiate(policeTapeBodyPrefab);
                    tapeClone.transform.position = evidenceItem.position - new Vector3(0, 1, 0);
                    tapeClone.transform.eulerAngles = new Vector3(0, Random.Range(-360, 360), 0);
                }
                if (evidenceItem.type == evidenceType.uniform)
                {
                    GameObject tapeClone = Instantiate(policeTapeItemPrefab);
                    tapeClone.transform.position = evidenceItem.position;
                    tapeClone.transform.eulerAngles = new Vector3(0, Random.Range(-360, 360), 0);

                    GameObject uniform = Instantiate(player.references.uniformPrefabs[0]);
                    uniform.transform.parent = null;
                    uniform.transform.position = evidenceItem.position;
                    uniform.transform.Find("Mesh").GetComponent<MeshFilter>().mesh = player.references.clothingMesh.sharedMesh;
                }
            }
        }
    }

    void LateUpdate()
    {
        if (lastCalled != policeCalled)
        {
            StartCoroutine(callPolice());
        }
        if (policeCalled)
        {
            if (currentPoliceState == 0)
            {
                timerRoot.rectTransform.anchoredPosition = Vector3.Lerp(timerRoot.rectTransform.anchoredPosition, new Vector3(5, -107.4f, 0), Time.deltaTime * 5);
                musicManager.saneMusic.volume = 1 - player.references.fadeManager.alphaGroup.alpha;
                musicManager.insaneMusic.volume = 1 - player.references.fadeManager.alphaGroup.alpha;
            }
        }
        if (policeSubtitleEnabled)
        {
            Color textColor = policeSubtitle.color;
            textColor.a = Mathf.Lerp(textColor.a, 1.1f, Time.deltaTime * 2);
            policeSubtitle.color = textColor;
        }
        else
        {
            Color textColor = policeSubtitle.color;
            textColor.a = Mathf.Lerp(textColor.a, -0.1f, Time.deltaTime * 2);
            policeSubtitle.color = textColor;
        }
        if (Input.GetKey(KeyCode.Backspace))
        {
            minute = 0;
            second = 1;
        }
        lastCalled = policeCalled;
    }

    IEnumerator callPolice()
    {
        SoundManager.Play("StudentCallPolice");
        musicManager.saneMusic.Stop();
        musicManager.insaneMusic.Stop();
        yield return new WaitForSeconds(6f);
        musicManager.saneMusic.clip = policeMusic[0];
        musicManager.insaneMusic.clip = policeMusic[0];
        musicManager.saneMusic.Play();
        musicManager.insaneMusic.Play();
        while (true)
        {
            yield return new WaitForSeconds(1f);
            updateClock();
            if (second == 0 && minute == 0) break;
        }
        StartCoroutine(interrogatePlayer());
    }

    public IEnumerator interrogatePlayer()
    {
        #region Beginning Transition
        #region Fade In
        player.stats.canMove = playerMovementMode.none;
        player.playAnimation(player.moveAnims.idleAnim, 0.1f);
        player.references.fadeManager.fadeSpeed = 4;
        player.references.fadeManager.fadeColor = Color.black;
        player.references.fadeManager.fade = true;
        player.references.rigidbody.isKinematic = true;
        player.references.fadeManager.fadeSpeed = 1;
        player.stats.promptsEnabled = false;
        player.showUI = false;
        if (player.references.inventoryManager.selected.myItem != null)
            player.references.inventoryManager.dropItem(player.references.inventoryManager.current);
        #endregion
        yield return new WaitForSeconds(2);
        #region Teleport player to room
        player.enabled = false;
        player.references.mainCamera.transform.position = interrogrationCameraPosition.position;
        player.references.mainCamera.transform.eulerAngles = interrogrationCameraPosition.eulerAngles;
        player.playAnimation("sit_0", 0f);
        player.stats.selectedStudent = interrogatorOpinion;
        player.transform.position = playerInterrogationRoomSitPosition.position;
        player.transform.eulerAngles = playerInterrogationRoomSitPosition.eulerAngles;
        player.updateSanity();
        #endregion

        #region Subtitles
        policeSubtitleEnabled = true;
        policeSubtitle.text = "The police arrive at school and do an investigation.";
        yield return new WaitUntil(() => Input.anyKey);
        policeSubtitleEnabled = false;
        yield return new WaitForSeconds(1);
        policeSubtitleEnabled = true;
        policeSubtitle.text = "The police start pulling students into a room to question them.";
        yield return new WaitUntil(() => Input.anyKey);
        policeSubtitleEnabled = false;
        yield return new WaitForSeconds(1);
        policeSubtitleEnabled = true;
        policeSubtitle.text = "After an hour, Uchikina is pulled into the room to be questioned.";
        yield return new WaitUntil(() => Input.anyKey);
        policeSubtitleEnabled = false;
        #endregion
        yield return new WaitForSeconds(2);
        #region Fade Out
        currentPoliceState = 1;
        interrogationRoom.SetActive(true);
        player.references.fadeManager.fade = false;
        musicManager.saneMusic.volume = 1;
        musicManager.insaneMusic.volume = 0;
        musicManager.saneMusic.clip = policeMusic[1];
        musicManager.insaneMusic.clip = policeMusic[1];
        musicManager.saneMusic.Play();
        musicManager.insaneMusic.Play();
        yield return new WaitForSeconds(1);
        #endregion
        #endregion

        #region Questions
        TalkUI tm = player.references.talkManager;
        int iDidItCount = 0;

        #region Welcome
        tm.activate("Interrogator");
        tm.fillDecay = false;
        tm.setText("Hello. We're just here to ask you some questions.");
        tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
        tm.setText("We only have enough time for 3 questions per student, so this won't take long.");
        tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
        #endregion

        interrogatorAnimator.CrossFadeInFixedTime("CrossArms", 0.5f);

        #region First Question
        if (player.stats.bloodiness > 0 && player.stats.sanity < 3f)
        {
            #region Question
            tm.setText("You're our prime suspect judging by your current... look and behavior. Care to explain yourself?");
            tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
            tm.setChoices("I murdered them.", "This is ketchup.", "Blood got on me, and I'm scared.", "...", null);
            #endregion

            tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
            tm.hideChoices();

            #region Response
            switch (tm.currentSelected)
            {
                case 0:
                    tm.setText("THIS ISN'T A JOKE. What you're saying is incredibly alarming. You better not try anything.");
                    iDidItCount++;
                    interrogatorAnimator.CrossFadeInFixedTime("SlamDesk", 0.5f);
                    SoundManager.Play("Interrogate_DeskSlam");
                    interrogatorOpinion.playerOpinion -= 0.5f;
                    break;
                case 1:
                    tm.setText("THIS ISN'T A JOKE. If you're trying to be funny, it's not working.");
                    interrogatorAnimator.CrossFadeInFixedTime("SlamIdle_Calm", 0.5f);
                    interrogatorOpinion.playerOpinion -= 0.15f;
                    break;
                case 2:
                    tm.setText("Hm...");
                    interrogatorOpinion.playerOpinion -= 0.5f;
                    tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
                    tm.setText("I guess that makes sense.");
                    interrogatorAnimator.CrossFadeInFixedTime("Think", 0.5f);
                    interrogatorOpinion.playerOpinion += 0.5f;
                    break;
                case 3:
                    tm.setText("Staying silent, huh? Pleading the fifth?");
                    interrogatorOpinion.playerOpinion -= 0.5f;
                    break;
            }
            #endregion
        }
        else if (player.stats.bloodiness > 0)
        {
            #region Question
            tm.setText("The first question should be obvious. Why do you have blood on you?");
            tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
            tm.setChoices("I murdered them.", "This is ketchup.", "Blood got on me.", "It's none of your business.", null);
            #endregion

            tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
            tm.hideChoices();

            #region Response
            switch (tm.currentSelected)
            {
                case 0:
                    tm.setText("THIS ISN'T A JOKE. What you're saying is incredibly alarming. You better not try anything.");
                    interrogatorAnimator.CrossFadeInFixedTime("SlamDesk", 0.5f);
                    SoundManager.Play("Interrogate_DeskSlam");
                    interrogatorOpinion.playerOpinion -= 0.5f;
                    break;
                case 1:
                    tm.setText("THIS ISN'T A JOKE. If you're trying to be funny, it's not working.");
                    interrogatorAnimator.CrossFadeInFixedTime("SlamIdle_Calm", 0.5f);
                    interrogatorOpinion.playerOpinion -= 0.15f;
                    break;
                case 2:
                    tm.setText("Hm...");
                    interrogatorOpinion.playerOpinion -= 0.5f;
                    tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
                    tm.setText("I guess that makes sense.");
                    interrogatorAnimator.CrossFadeInFixedTime("Think", 0.5f);
                    interrogatorOpinion.playerOpinion += 0.5f;
                    break;
                case 3:
                    tm.setText("This is a school murder case. This is definitely my business. Being stubborn is only going to make you look even more suspicious.");
                    interrogatorAnimator.CrossFadeInFixedTime("SlamIdle_Calm", 0.5f);
                    interrogatorOpinion.playerOpinion -= 0.25f;
                    break;
            }
            #endregion
        }
        else if (player.stats.sanity < 3f)
        {
            #region Question
            tm.setText("The first question should be obvious. You look troubled. What's going on?");
            tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
            tm.setChoices("I murdered them.", "I'm just scared...", "I saw the body.", "It's cold in here.", null);
            #endregion

            tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
            tm.hideChoices();

            #region Response
            switch (tm.currentSelected)
            {
                case 0:
                    tm.setText("THIS ISN'T A JOKE. What you're saying is incredibly alarming. You better not try anything.");
                    interrogatorOpinion.playerOpinion -= 0.5f;
                    interrogatorAnimator.CrossFadeInFixedTime("SlamDesk", 0.5f);
                    SoundManager.Play("Interrogate_DeskSlam");
                    break;
                case 1:
                    tm.setText("Hm...");
                    interrogatorOpinion.playerOpinion -= 0.5f;
                    tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
                    tm.setText("So you're just tense from the event? That's okay. Happens all the time.");
                    interrogatorAnimator.CrossFadeInFixedTime("Sad", 0.5f);
                    interrogatorOpinion.playerOpinion += 0.65f;
                    break;
                case 2:
                    tm.setText("Oh. I see. You must be shaken up from it. Sorry for asking.");
                    interrogatorAnimator.CrossFadeInFixedTime("Sad", 0.5f);
                    interrogatorOpinion.playerOpinion += 0.5f;
                    break;
                case 3:
                    tm.setText("Hmm.");
                    interrogatorOpinion.playerOpinion -= 0.05f;
                    break;
            }
            #endregion
        }
        else
        {
            #region Question
            tm.setText("The first thing I'd like to know is if you know what happened.");
            tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
            tm.setChoices("I murdered them.", "I don't.", "Yeah, I know.", "...", null);
            #endregion

            tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
            tm.hideChoices();

            #region Response
            switch (tm.currentSelected)
            {
                case 0:
                    tm.setText("THIS ISN'T A JOKE. What you're saying is incredibly alarming. You better not try anything.");
                    interrogatorOpinion.playerOpinion -= 0.5f;
                    interrogatorAnimator.CrossFadeInFixedTime("SlamDesk", 0.5f);
                    SoundManager.Play("Interrogate_DeskSlam");
                    break;
                case 1:
                    tm.setText("Apparently, " + (studentsKilled.Count > 1 ? studentsKilled.Count + " students were" : "one student was") + " murdered at your school, Sutoka High.");
                    interrogatorOpinion.playerOpinion -= 0.05f;
                    interrogatorAnimator.CrossFadeInFixedTime("Sad", 0.5f);
                    break;
                case 2:
                    interrogatorOpinion.playerOpinion += 0.05f;
                    break;
                case 3:
                    tm.setText("Are you okay? I know it was tragic, but we need answers.");
                    interrogatorOpinion.playerOpinion += 0.05f;
                    break;
            }
            #endregion
        }
        #endregion

        tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
        tm.setText("Next question...");
        interrogatorAnimator.CrossFadeInFixedTime("CrossArms", 0.5f);
        tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);

        #region Second Question
        if (weaponsUsed.Count > 0 && weaponsUsed[0].name != "DestroyedObject")
        {
            #region Question
            interrogatorAnimator.CrossFadeInFixedTime("SlamIdle_Calm", 0.5f);
            tm.setText("We found a " + weaponsUsed[0].name + " with your fingerprints and with the blood of the victim on it. Do you have any excuse?");
            tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
            tm.setChoices("I murdered them.", "I've been framed!", "Those aren't my fingerprints!", "I touched it, but I didn't use it on someone!", null);
            #endregion

            tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
            tm.hideChoices();

            #region Response
            switch (tm.currentSelected)
            {
                case 0:
                    interrogatorAnimator.CrossFadeInFixedTime("SlamDesk", 0.5f);
                    SoundManager.Play("Interrogate_DeskSlam");
                    if (iDidItCount == 0)
                    {
                        tm.setText("THIS ISN'T A JOKE. What you're saying is incredibly alarming. You better not try anything.");
                    }
                    else
                    {
                        tm.setText("I'll repeat myself. That is a VERY serious statement. I don't think you know what you're doing.");
                    }
                    interrogatorOpinion.playerOpinion -= 0.5f;
                    break;
                case 1:
                    interrogatorAnimator.CrossFadeInFixedTime("Sad", 0.5f);
                    tm.setText("Hmm...");
                    interrogatorOpinion.playerOpinion -= 0.05f;
                    break;
                case 2:
                    tm.setText("...");
                    interrogatorAnimator.CrossFadeInFixedTime("Sad", 0.5f);
                    interrogatorOpinion.playerOpinion += 0.5f;
                    tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
                    tm.setText("You didn't even see the fingerprints yet...");
                    interrogatorAnimator.CrossFadeInFixedTime("Think", 0.5f);
                    interrogatorOpinion.playerOpinion -= 1f;
                    break;
                case 3:
                    interrogatorAnimator.CrossFadeInFixedTime("Think", 0.5f);
                    tm.setText("So you admit you touched it. Alright.");
                    interrogatorOpinion.playerOpinion += 0.05f;
                    break;
            }
            #endregion
        }
        else if (bloodyUniforms.Count > 0 && bloodyUniforms[0].name != "DestroyedObject")
        {
            #region Question
            interrogatorAnimator.CrossFadeInFixedTime("CrossArms", 0.5f);
            tm.setText("We found a bloody uniform which had your name on it. Do you have an excuse?");
            tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
            tm.setChoices("I murdered them.", "I've been framed!", "I'm on my period.", "...", null);
            #endregion

            tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
            tm.hideChoices();

            #region Response
            switch (tm.currentSelected)
            {
                case 0:
                    interrogatorAnimator.CrossFadeInFixedTime("SlamDesk", 0.5f);
                    SoundManager.Play("Interrogate_DeskSlam");
                    if (iDidItCount == 0)
                    {
                        tm.setText("THIS ISN'T A JOKE. What you're saying is incredibly alarming. You better not try anything.");
                    }
                    else
                    {
                        tm.setText("I'll repeat myself. That is a VERY serious statement. I don't think you know what you're doing.");
                    }
                    interrogatorOpinion.playerOpinion -= 0.5f;
                    break;
                case 1:
                    interrogatorAnimator.CrossFadeInFixedTime("Sad", 0.5f);
                    tm.setText("Hmm...");
                    interrogatorOpinion.playerOpinion -= 0.05f;
                    break;
                case 2:
                    tm.setText("...");
                    interrogatorOpinion.playerOpinion -= 0.05f;
                    break;
                case 3:
                    tm.setText("...");
                    interrogatorOpinion.playerOpinion -= 0.05f;
                    break;
            }
            #endregion
        }
        else
        {
            #region Question
            interrogatorAnimator.CrossFadeInFixedTime("Sad", 0.5f);
            tm.setText("Where were you during the time of death?");
            tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
            tm.setChoices("I murdered them.", "Nowhere near.", "At home.", "At class.", "Hanging out.");
            #endregion

            tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
            tm.hideChoices();

            #region Response
            switch (tm.currentSelected)
            {
                case 0:
                    interrogatorAnimator.CrossFadeInFixedTime("SlamDesk", 0.5f);
                    SoundManager.Play("Interrogate_DeskSlam");
                    if (iDidItCount == 0)
                    {
                        tm.setText("THIS ISN'T A JOKE. What you're saying is incredibly alarming. You better not try anything.");
                    }
                    else
                    {
                        tm.setText("...");
                    }
                    interrogatorOpinion.playerOpinion -= 0.5f;
                    break;
                case 1:
                    interrogatorAnimator.CrossFadeInFixedTime("Think", 0.5f);
                    tm.setText("Nowhere near, huh?");
                    interrogatorOpinion.playerOpinion -= 0.05f;
                    break;
                case 2:
                    tm.setText("...");
                    interrogatorOpinion.playerOpinion -= 0.5f;
                    break;
                case 3:
                    // Lower the opinion if the victim didn't die at classtime
                    tm.setText("...");
                    interrogatorOpinion.playerOpinion -= 0.5f;
                    break;
                case 4:
                    // Lower the opinion if the victim didn't die at classtime
                    tm.setText("...");
                    interrogatorOpinion.playerOpinion += 0.5f;
                    break;
            }
            #endregion
        }
        #endregion

        tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
        tm.setText("Last question...");
        interrogatorAnimator.CrossFadeInFixedTime("CrossArms", 0.5f);
        tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);

        #region Third Question
        if (studentsKilled[0].name != "DestroyedObject")
        {
            #region Question
            interrogatorAnimator.CrossFadeInFixedTime("Sad", 0.5f);
            string wantedString = studentsKilled.Count <= 1 ? "The student who was named " : "One of the students who were killed were named ";
            tm.setText(wantedString + studentsKilled[0].name + ". Do you know who they are?");
            tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
            tm.setChoices("I murdered them.", "Yes.", "No.", null, null);
            #endregion

            tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
            tm.hideChoices();

            #region Response
            switch (tm.currentSelected)
            {
                case 0:
                    interrogatorAnimator.CrossFadeInFixedTime("SlamDesk", 0.5f);
                    SoundManager.Play("Interrogate_DeskSlam");
                    if (iDidItCount == 0)
                    {
                        tm.setText("THIS ISN'T A JOKE. What you're saying is incredibly alarming. You better not try anything.");
                    }
                    else
                    {
                        tm.setText("...");
                    }
                    interrogatorOpinion.playerOpinion -= 0.5f;
                    break;
                case 1:
                    interrogatorAnimator.CrossFadeInFixedTime("Think", 0.5f);
                    tm.setText("...");
                    if (studentsKilled[0].metPlayer)
                        interrogatorOpinion.playerOpinion += 0.05f;
                    else
                        interrogatorOpinion.playerOpinion -= 0.05f;
                    break;
                case 2:
                    interrogatorAnimator.CrossFadeInFixedTime("Think", 0.5f);
                    tm.setText("...");
                    if (!studentsKilled[0].metPlayer)
                        interrogatorOpinion.playerOpinion += 0.05f;
                    else
                        interrogatorOpinion.playerOpinion -= 0.05f;
                    break;
            }
            #endregion
        }
        else if (murderWitnesses > 3)
        {
            #region Question
            interrogatorAnimator.CrossFadeInFixedTime("CrossArms", 0.5f);
            tm.setText("Numerous people have said the murderer is you. Did you do it?");
            tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
            tm.setChoices("Yes.", "No.", null, null, null);
            #endregion

            tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
            tm.hideChoices();

            #region Response
            switch (tm.currentSelected)
            {
                case 0:
                    interrogatorAnimator.CrossFadeInFixedTime("Think", 0.5f);
                    tm.setText("...");
                    interrogatorOpinion.playerOpinion -= 1f;
                    break;
                case 1:
                    interrogatorAnimator.CrossFadeInFixedTime("Think", 0.5f);
                    tm.setText("...");
                    interrogatorOpinion.playerOpinion -= 1f;
                    break;
            }
            #endregion
        }
        #endregion
        #endregion

        #region End Results
        tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
        if (interrogatorOpinion.playerOpinion < 0.6f)
        {
            interrogatorAnimator.CrossFadeInFixedTime("Sad", 0.5f);
            tm.setText("I see. Thank you for your answers.");
            tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
            SoundManager.Play("Interrogate_Fail");
            interrogatorAnimator.CrossFadeInFixedTime("AimGun", 0.5f);
            policeGun.enabled = true;
            tm.setText("You're under arrest!");
        }
        else
        {
            interrogatorAnimator.CrossFadeInFixedTime("Sad", 0.5f);
            tm.setText("I see. Thank you for your answers.");
            tm.waitForInput(); yield return new WaitUntil(() => tm.playerInputted == true);
            tm.setText("You're free to go.");
        }
        #endregion
    }

    void updateClock()
    {
        second--;
        if (second < 0)
        {
            minute--;
            second = 59;
        }
        timerText.text = minute + (second < 10 ? ":0" : ":") + second;
    }
}