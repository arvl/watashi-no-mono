﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class PackageManager : MonoBehaviour {
    public static void loadPackages()
    {
        string path = Application.streamingAssetsPath + "/Packages";

        foreach (string file in System.IO.Directory.GetFiles(path))
        {
            if (!file.Contains(".meta") && !file.Contains(".txt"))
            {
                importPackage(file.Replace("\\", "/"));
            }
        }
    }

    static void importPackage(string packagePath)
    {
        // Find the package from the Packages folder
        AssetBundle assetBundle = AssetBundle.LoadFromFile(packagePath);
        if (assetBundle != null)
        {
            // Read the txt with the same name of the package file
            string path = packagePath + ".txt";

            if (File.Exists(path))
            {
                try
                {
                    string[] lines = System.IO.File.ReadAllLines(path);
                    foreach (string line in lines)
                    {
                        var prefab = assetBundle.LoadAsset<GameObject>(line);
                    }
                    assetBundle.Unload(false);
                    Debug.Log("Successfully imported package: " + packagePath);
                }
                catch
                {
                    Debug.Log("Could not find or read package: " + packagePath);
                }
            }
            else
            {
                Debug.Log("Could not find or read package: " + packagePath);
            }
        }
        else
        {
            Debug.Log("Could not find or read package: " + packagePath);
        }
    }
}
