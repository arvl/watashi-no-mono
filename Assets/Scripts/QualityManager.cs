﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;
using UnityEngine.SceneManagement;

public class QualityManager : MonoBehaviour {
    public static int getQuality()
    {
        return QualitySettings.GetQualityLevel();
    }

    public static void setQuality(int qualityLevel)
    {
        QualitySettings.SetQualityLevel(qualityLevel);
    }

    public static void resetSettings()
    {
        string path = Application.streamingAssetsPath + "/Preferences/Settings.txt";
        FileStream file;

        if (File.Exists(path))
        {
            File.Delete(path);
            file = File.Create(path);
            file.Close();
        }

        gameSettings settingsInfo = new gameSettings();
        settingsInfo.mouseSensitivity = 8f;
        settingsInfo.playerScaleTest = 1f;
        settingsInfo.studentScaleMultiplier = 1f;
        settingsInfo.musicVolume = 0.5f;
        settingsInfo.voiceVolume = 1f;
        settingsInfo.effectVolume = 1f;
        settingsInfo.shadowsEnabled = true;
        settingsInfo.enableUI = true;

        string json = JsonUtility.ToJson(settingsInfo);

        StreamWriter writer = new StreamWriter(path, true);
        writer.Write(json);
        writer.Close();
    }

    public static void loadSettings()
    {
        string path = Application.streamingAssetsPath + "/Preferences/Settings.txt";

        if (!File.Exists(path))
        {
            resetSettings();
        }

        gameSettings settingsInfo = null;

        StreamReader reader = new StreamReader(path);
        settingsInfo = JsonUtility.FromJson<gameSettings>(reader.ReadToEnd());
        reader.Close();

        if (settingsInfo != null)
        {
            QualitySettings.masterTextureLimit = settingsInfo.textureDivide;
            PlayerPrefs.SetFloat("mouseSensitivity", settingsInfo.mouseSensitivity);
            PlayerPrefs.SetFloat("musicVolume", settingsInfo.musicVolume);
            PlayerPrefs.SetFloat("voiceVolume", settingsInfo.voiceVolume);
            PlayerPrefs.SetFloat("effectVolume", settingsInfo.effectVolume);
            PlayerPrefs.SetInt("debugHotkeysEnabled", settingsInfo.debugHotkeysEnabled ? 1 : 0);
            PlayerPrefs.SetInt("debugConsoleEnabled", settingsInfo.debugConsoleEnabled ? 1 : 0);
            GameObject.Find("MainCamera").GetComponent<Camera>().farClipPlane = settingsInfo.cameraFar;
            PlayerPrefs.SetInt("imageEffects", settingsInfo.imageEffects ? 0 : 1);
            GameObject.Find("Uchikina").transform.localScale = new Vector3(settingsInfo.playerScaleTest * 0.4f, settingsInfo.playerScaleTest * 0.4f, settingsInfo.playerScaleTest * 0.4f);
            GameObject.Find("UI").SetActive(settingsInfo.enableUI);
            GameObject.Find("Sun").SetActive(settingsInfo.sunEnabled);
            if (!settingsInfo.shadowsEnabled)
            {
                GameObject.Find("Sun").GetComponent<Light>().shadows = LightShadows.None;
            }
            foreach (Student stud in GameObject.FindObjectsOfType<Student>())
            {
                float size = stud.transform.localScale.y * settingsInfo.studentScaleMultiplier;
                stud.transform.localScale = new Vector3(size, size, size);
            }
            foreach (ParticleSystem particle in GameObject.FindObjectsOfType<ParticleSystem>())
            {
                particle.gameObject.SetActive(settingsInfo.particlesEnabled);
            }
            foreach (Animator anim in GameObject.FindObjectsOfType<Animator>())
            {
                anim.enabled = settingsInfo.animationsEnabled;
            }
            if (settingsInfo.importPackages) PackageManager.loadPackages();
        }
        else
        {
            throw new System.Exception("Could not read settings!");
        }
    }
}
