﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    [SerializeField] GameObject menuWalls;
    [SerializeField] Camera mainCamera;
    [SerializeField] Animator cameraAnimator;
    [SerializeField] Animator Logo;
    [SerializeField] Animator uchiAnimator;
    [SerializeField] Animator bookAnimator;
    [SerializeField] Transform handIK;
    [SerializeField] AudioSource src;
    [SerializeField] AudioClip[] sounds;
    [SerializeField] Canvas[] bookScreens;
    [SerializeField] Transform[] playerPositions;
    [SerializeField] Animator UIAnimator;
    [SerializeField] Canvas UICanvas;
    [SerializeField] Renderer[] buttonOverlay;
    [SerializeField] AudioSource music;

    [SerializeField] int currentSaveSelected;

    [SerializeField] RectTransform savePanelSelector;
    [SerializeField] RectTransform[] savePanelButtons;

    [SerializeField] bool settingsPanelRight;
    [SerializeField] bool settingsSelected;
    [SerializeField] RectTransform settingsPanelSelector;
    [SerializeField] RectTransform[] leftSettingsPanelButtons;
    [SerializeField] RectTransform[] rightSettingsPanelButtons;
    [SerializeField] RectTransform backButton;

    [SerializeField] Transform[] cameraPositions;
    [SerializeField] Transform[] handPositions;

    [SerializeField] bool skippedIntro;
    [SerializeField] bool beginningOver;

    [SerializeField] int current = 0;
    [SerializeField] int currentSelected = -1;

	void Start () {
        UICanvas.enabled = false;
        if (!PlayerPrefs.HasKey("MasterVolume"))
        {
            PlayerPrefs.SetFloat("MasterVolume",    1f);
            PlayerPrefs.SetFloat("MusicVolume",     1f);
            PlayerPrefs.SetFloat("SoundVolume",     1f);
        }
        rightSettingsPanelButtons[0].Find("Slider").GetComponent<Slider>().value = PlayerPrefs.GetFloat("MasterVolume");
        rightSettingsPanelButtons[1].Find("Slider").GetComponent<Slider>().value = PlayerPrefs.GetFloat("MusicVolume");
        rightSettingsPanelButtons[2].Find("Slider").GetComponent<Slider>().value = PlayerPrefs.GetFloat("SoundVolume");
        src = GetComponent<AudioSource>();
        mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        cameraAnimator = mainCamera.GetComponent<Animator>();
        Logo = GameObject.Find("Logo").GetComponent<Animator>();
        menuWalls.SetActive(false);
        bookScreens[3].enabled = false;

        StartCoroutine(Init());
	}

    IEnumerator Init()
    {
        music.Play();
        yield return new WaitForSeconds(2.15f);
        if (!skippedIntro)
        {
            Logo.Play("NewMenuIntro_Knife");
        }
        yield return new WaitForSeconds(27.85f);
        if (!skippedIntro)
        {
            UIAnimator.CrossFadeInFixedTime("NewMenuIntro_Menu_Enter", 0.1f);
            UICanvas.enabled = true;
            UICanvas.transform.Find("Skip").gameObject.SetActive(false);
            beginningOver = true;
            Logo.speed = 999999;
        }
    }

    void Update() {
        UICanvas.enabled = false;
        buttonOverlay[0].enabled = false;
        buttonOverlay[1].enabled = false;
        buttonOverlay[2].enabled = false;
        buttonOverlay[3].enabled = false;

        buttonOverlay[4].enabled = false;
        buttonOverlay[5].enabled = false;
        buttonOverlay[6].enabled = false;
        buttonOverlay[7].enabled = false;
        if (currentSelected == -1)
        {
            uchiAnimator.transform.position = Vector3.Lerp(uchiAnimator.transform.position, playerPositions[0].position, Time.deltaTime * 8);
            buttonOverlay[current].enabled = true;
        }
        else
        {
            if (currentSelected == 0)
            {
                buttonOverlay[current + 4].enabled = true;
            }
            uchiAnimator.transform.position = Vector3.Lerp(uchiAnimator.transform.position, playerPositions[1].position, Time.deltaTime * 8);
        }
        if (beginningOver)
        {
            if (currentSelected != 1)
            {
                if (currentSelected != 4)
                {
                    // Textbook controls
                    if (Input.GetKeyDown(KeyCode.W))
                    {
                        if (current == 0)
                        {
                            current = 3;
                            src.PlayOneShot(sounds[0]);
                        }
                        else
                        {
                            current -= 1;
                            src.PlayOneShot(sounds[0]);
                        }
                    }
                    if (Input.GetKeyDown(KeyCode.S))
                    {
                        if (current == 3)
                        {
                            current = 0;
                            src.PlayOneShot(sounds[0]);
                        }
                        else
                        {
                            current += 1;
                            src.PlayOneShot(sounds[0]);
                        }
                    }
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        switch (current)
                        {
                            case 0:
                                if (currentSelected == -1)
                                {
                                    // Play button
                                    currentSelected = 0;
                                    bookAnimator.Play("NewMenuIntro_Book_0");
                                    UIAnimator.CrossFadeInFixedTime("NewMenuIntro_Saves_Enter", 0.1f);
                                    uchiAnimator.CrossFade("f01_sit_01", 0.15f);
                                    src.PlayOneShot(sounds[1]);
                                    bookScreens[0].enabled = false;
                                    bookScreens[1].enabled = true;
                                    prepareSaveList(1);
                                    prepareSaveList(2);
                                    prepareSaveList(3);
                                }
                                else if (currentSelected == 0)
                                {
                                    // Open save 1
                                    currentSelected = 4;
                                    if (SaveManager.saveExists(1) == saveStatus.Working)
                                    {
                                        current = 0;
                                    }
                                    else
                                    {
                                        current = 1;
                                    }
                                    UIAnimator.CrossFadeInFixedTime("NewMenuIntro_Menu_Exit", 0.1f);
                                    currentSaveSelected = 1;
                                    src.PlayOneShot(sounds[1]);
                                    bookScreens[2].enabled = true;
                                    prepareSavePanel(1);
                                }
                                break;
                            case 1:
                                if (currentSelected == -1)
                                {
                                    // Settings
                                    UIAnimator.CrossFadeInFixedTime("NewMenuIntro_Settings_Enter", 0.1f);
                                    currentSelected = 1;
                                    src.PlayOneShot(sounds[1]);
                                    bookScreens[3].enabled = true;
                                    menuWalls.SetActive(true);
                                }
                                else if (currentSelected == 0)
                                {
                                    // Open save 2
                                    currentSelected = 4;
                                    if (SaveManager.saveExists(2) == saveStatus.Working)
                                    {
                                        current = 0;
                                    }
                                    else
                                    {
                                        current = 1;
                                    }
                                    UIAnimator.CrossFadeInFixedTime("NewMenuIntro_Menu_Exit", 0.1f);
                                    currentSaveSelected = 2;
                                    src.PlayOneShot(sounds[1]);
                                    bookScreens[2].enabled = true;
                                    prepareSavePanel(2);
                                }
                                break;
                            case 2:
                                if (currentSelected == -1)
                                {
                                    // Credits
                                    src.PlayOneShot(sounds[1]);
                                }
                                else if (currentSelected == 0)
                                {
                                    // Open save 3
                                    currentSelected = 4;
                                    if (SaveManager.saveExists(3) == saveStatus.Working)
                                    {
                                        current = 0;
                                    }
                                    else
                                    {
                                        current = 1;
                                    }
                                    UIAnimator.CrossFadeInFixedTime("NewMenuIntro_Menu_Exit", 0.1f);
                                    currentSaveSelected = 3;
                                    src.PlayOneShot(sounds[1]);
                                    bookScreens[2].enabled = true;
                                    prepareSavePanel(3);
                                }
                                break;
                            case 3:
                                if (currentSelected == -1)
                                {
                                    // Exit
                                    currentSelected = 4;
                                    src.PlayOneShot(sounds[1]);
                                    //beginningOver = false;
                                    UIAnimator.CrossFadeInFixedTime("NewMenuIntro_Menu_Exit", 0.1f);
                                    skippedIntro = true;
                                    Invoke("quitGame", 1f);
                                }
                                else
                                {
                                    // Exit out of whatever is open, except for the save UI and settings menu
                                    if (currentSelected < 4 && currentSelected != 1)
                                    {
                                        if (currentSelected == 0)
                                        {
                                            UIAnimator.CrossFadeInFixedTime("NewMenuIntro_Saves_Exit", 0.1f);
                                        }
                                        currentSelected = -1;
                                        bookAnimator.Play("NewMenuIntro_Book_1");
                                        uchiAnimator.CrossFade("f02_sit_01 0", 0.15f);
                                        src.PlayOneShot(sounds[1]);
                                        bookScreens[0].enabled = true;
                                        bookScreens[1].enabled = false;
                                    }
                                }
                                break;
                        }
                        if (currentSelected != 4)
                        {
                            current = 0;
                        }
                    }
                }
                else
                {
                    // Save panel controls
                    if (Input.GetKeyDown(KeyCode.W))
                    {
                        if (SaveManager.saveExists(currentSaveSelected) == saveStatus.Working)
                        {
                            if (current == 0)
                            {
                                current = 3;
                                src.PlayOneShot(sounds[0]);
                            }
                            else if (current == 2)
                            {
                                current = 0;
                            }
                            else
                            {
                                current -= 1;
                                src.PlayOneShot(sounds[0]);
                            }
                        }
                        else
                        {
                            if (current == 1)
                            {
                                current = 3;
                                src.PlayOneShot(sounds[0]);
                            }
                            else if (current == 3)
                            {
                                current = 1;
                            }
                            else
                            {
                                current -= 1;
                                src.PlayOneShot(sounds[0]);
                            }
                        }
                    }
                    if (Input.GetKeyDown(KeyCode.S))
                    {
                        if (SaveManager.saveExists(currentSaveSelected) == saveStatus.Working)
                        {
                            if (current == 3)
                            {
                                current = 0;
                                src.PlayOneShot(sounds[0]);
                            }
                            else if (current == 0)
                            {
                                current = 2;
                            }
                            else
                            {
                                current += 1;
                                src.PlayOneShot(sounds[0]);
                            }
                        }
                        else
                        {
                            if (current == 3)
                            {
                                current = 1;
                                src.PlayOneShot(sounds[0]);
                            }
                            else if (current == 1)
                            {
                                current = 3;
                            }
                            else
                            {
                                current += 1;
                                src.PlayOneShot(sounds[0]);
                            }
                        }
                    }
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        switch (current)
                        {
                            case 0:
                                // Load and play the game if things are working well
                                src.PlayOneShot(sounds[1]);
                                if (SaveManager.saveExists(currentSaveSelected) == saveStatus.Working)
                                {
                                    SaveManager.loadGame(currentSaveSelected);
                                    saveData data = SaveManager.getSaveData(currentSaveSelected);
                                    PlayerPrefs.SetInt("CurrentSaveFile", currentSaveSelected);
                                    if (data.am == 1)
                                    {
                                        SceneManager.LoadScene("BedroomNight");
                                    }
                                    else
                                    {
                                        SceneManager.LoadScene("Bedroom");
                                    }
                                }
                                break;
                            case 1:
                                // Create new game
                                current = 0;
                                src.PlayOneShot(sounds[1]);
                                PlayerPrefs.DeleteAll();
                                PlayerPrefs.SetFloat("MasterVolume", 1f);
                                PlayerPrefs.SetFloat("MusicVolume", 1f);
                                PlayerPrefs.SetFloat("SoundVolume", 1f);
                                PlayerPrefs.SetInt("StudentsKilled", 0);
                                PlayerPrefs.SetFloat("PlayerStrength", 0);
                                PlayerPrefs.SetFloat("PlayerSpeed", 0);
                                PlayerPrefs.SetFloat("PlayerSeduction", 0);
                                PlayerPrefs.SetInt("Night", 1);
                                PlayerPrefs.SetInt("PlayerTired", 9);
                                PlayerPrefs.SetInt("Week", 1);
                                PlayerPrefs.SetInt("DayID", 1);
                                SaveManager.saveGame(currentSaveSelected);
                                prepareSavePanel(currentSaveSelected);
                                break;
                            case 2:
                                // Erase save
                                SaveManager.eraseSave(currentSaveSelected);
                                currentSelected = 0;
                                current = 0;
                                bookAnimator.Play("NewMenuIntro_Book_0");
                                uchiAnimator.CrossFade("f01_sit_01", 0.15f);
                                src.PlayOneShot(sounds[1]);
                                bookScreens[2].enabled = false;
                                UIAnimator.CrossFadeInFixedTime("NewMenuIntro_Saves_Resume", 0.1f);
                                prepareSaveList(1);
                                prepareSaveList(2);
                                prepareSaveList(3);
                                break;
                            case 3:
                                // Go back to the textbook
                                UIAnimator.CrossFadeInFixedTime("NewMenuIntro_Saves_Resume", 0.1f);
                                currentSelected = 0;
                                current = 0;
                                bookAnimator.Play("NewMenuIntro_Book_0");
                                uchiAnimator.CrossFade("f01_sit_01", 0.15f);
                                src.PlayOneShot(sounds[1]);
                                bookScreens[2].enabled = false;
                                prepareSaveList(1);
                                prepareSaveList(2);
                                prepareSaveList(3);
                                break;
                        }
                    }
                }
            }
            else
            {
                // Settings menu controls
                if (!settingsSelected)
                {
                    if (Input.GetKeyDown(KeyCode.W))
                    {
                        if (current == 0)
                        {
                            current = 7;
                            src.PlayOneShot(sounds[0]);
                        }
                        else
                        {

                            if (settingsPanelRight)
                            {
                                if (current == 7)
                                {
                                    current = 2;
                                    src.PlayOneShot(sounds[0]);
                                }
                                else
                                {
                                    current -= 1;
                                    src.PlayOneShot(sounds[0]);
                                }
                            }
                            else
                            {
                                current -= 1;
                                src.PlayOneShot(sounds[0]);
                            }
                        }
                    }
                    if (Input.GetKeyDown(KeyCode.S))
                    {
                        if (current == 7)
                        {
                            current = 0;
                            src.PlayOneShot(sounds[0]);
                        }
                        else
                        {
                            if (settingsPanelRight)
                            {
                                if (current == 2)
                                {
                                    current = 7;
                                    src.PlayOneShot(sounds[0]);
                                }
                                else
                                {
                                    current += 1;
                                    src.PlayOneShot(sounds[0]);
                                }
                            }
                            else
                            {
                                current += 1;
                                src.PlayOneShot(sounds[0]);
                            }
                        }
                    }
                    if (Input.GetKeyDown(KeyCode.D))
                    {
                        current = 0;
                        settingsPanelRight = true;
                        src.PlayOneShot(sounds[0]);
                    }
                    if (Input.GetKeyDown(KeyCode.A))
                    {
                        current = 0;
                        settingsPanelRight = false;
                        src.PlayOneShot(sounds[0]);
                    }
                }
                else
                {
                    if (settingsPanelRight)
                    {
                        Slider audioSlider = rightSettingsPanelButtons[current].Find("Slider").GetComponent<Slider>();
                        if (Input.GetKey(KeyCode.D))
                        {
                            audioSlider.value += Time.deltaTime / 2;
                            if (current == 0)
                            {
                                PlayerPrefs.SetFloat("MasterVolume", audioSlider.value);
                            }
                            else if (current == 1)
                            {
                                PlayerPrefs.SetFloat("MusicVolume", audioSlider.value);
                            }
                            else
                            {
                                PlayerPrefs.SetFloat("SoundVolume", audioSlider.value);
                            }
                        }
                        if (Input.GetKey(KeyCode.A))
                        {
                            audioSlider.value -= Time.deltaTime / 2;
                            if (current == 0)
                            {
                                PlayerPrefs.SetFloat("MasterVolume", audioSlider.value);
                            }
                            else if (current == 1)
                            {
                                PlayerPrefs.SetFloat("MusicVolume", audioSlider.value);
                            }
                            else
                            {
                                PlayerPrefs.SetFloat("SoundVolume", audioSlider.value);
                            }
                        }
                    }
                }
                if (Input.GetKeyDown(KeyCode.E))
                {
                    if (current == 7)
                    {
                        // Go back to the textbook
                        currentSelected = -1;
                        current = 0;
                        bookAnimator.Play("NewMenuIntro_Book_1");
                        uchiAnimator.CrossFade("f02_sit_01 0", 0.15f);
                        src.PlayOneShot(sounds[1]);
                        bookScreens[0].enabled = true;
                        bookScreens[3].enabled = false;
                        UIAnimator.CrossFadeInFixedTime("NewMenuIntro_Settings_Exit", 0.1f);
                    }
                    else
                    {
                        src.PlayOneShot(sounds[1]);
                        settingsSelected = !settingsSelected;
                    }
                }
            }
        }
        if (currentSelected != 1)
        {
            if (currentSelected != 4)
            {
                if (currentSelected == -1)
                {
                    handIK.position = Vector3.Lerp(handIK.position, handPositions[current].position, Time.deltaTime * 8);
                    handIK.eulerAngles = Vector3.Lerp(handIK.eulerAngles, handPositions[current].eulerAngles, Time.deltaTime * 8);
                }
                else
                {
                    handIK.position = Vector3.Lerp(handIK.position, handPositions[current + 4].position, Time.deltaTime * 8);
                    handIK.eulerAngles = Vector3.Lerp(handIK.eulerAngles, handPositions[current + 4].eulerAngles, Time.deltaTime * 8);
                }
                if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.E))
                {
                    if (!skippedIntro && !beginningOver)
                    {
                        UIAnimator.CrossFadeInFixedTime("NewMenuIntro_Menu_Enter", 0.1f);
                        UICanvas.transform.Find("Skip").gameObject.SetActive(false);
                        skippedIntro = true;
                        UICanvas.enabled = true;
                        beginningOver = true;
                        Logo.speed = 999999;
                    }
                }
            }
            else
            {
                savePanelSelector.position = savePanelButtons[current].position;
            }
        }
        else
        {
            if (current != 7)
            {
                if (!settingsPanelRight)
                {
                    settingsPanelSelector.position = leftSettingsPanelButtons[current].position;
                }
                else
                {
                    if (current < 3)
                    {
                        settingsPanelSelector.position = rightSettingsPanelButtons[current].position;
                    }
                }
            }
            else
            {
                settingsPanelSelector.position = backButton.position;
            }
        }
        if (beginningOver) {
            cameraAnimator.enabled = false;
            Transform target = cameraPositions[0];
            switch (currentSelected)
            {
                case 0:
                    target = cameraPositions[1];
                    break;
                case 1:
                    target = cameraPositions[3];
                    break;
                case 2:
                    target = cameraPositions[1];
                    break;
                case 3:

                    break;
                case 4:
                    target = cameraPositions[2];
                    break;
                case 999:
                    target = cameraPositions[2];
                    break;
            }
            mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, target.position, Time.deltaTime * 4f);
            mainCamera.transform.eulerAngles = Vector3.Lerp(mainCamera.transform.eulerAngles, target.eulerAngles, Time.deltaTime * 5f);
        }
        UICanvas.enabled = true;
    }

    void quitGame()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit ();
        #endif
    }

    void prepareSaveList(int saveID)
    {
        Transform savePanel = bookScreens[1].transform;
        if (SaveManager.saveExists(saveID) == saveStatus.Working)
        {
            savePanel.Find("Save" + saveID.ToString()).GetComponent<Text>().text = "FILE " + saveID.ToString();
        }
        else if (SaveManager.saveExists(saveID) == saveStatus.Corrupt)
        {
            savePanel.Find("Save" + saveID.ToString()).GetComponent<Text>().text = "CORRUPT FILE";
        }
        else
        {
            savePanel.Find("Save" + saveID.ToString()).GetComponent<Text>().text = "Empty file";
        }
    }

    void prepareSavePanel(int saveID)
    {
        Transform savePanel = bookScreens[2].transform;
        if (SaveManager.saveExists(saveID) == saveStatus.Working)
        {
            saveData data = SaveManager.getSaveData(saveID);
            savePanel.Find("SaveName").GetComponent<Text>().text = "FILE " + saveID;
            Text saveInfo = savePanel.Find("SaveInfo").GetComponent<Text>();
            if (data.am == 1)
            {
                saveInfo.text = "Week " + data.week + ", " + data.day + ", 6:00 AM";
            }
            else
            {
                saveInfo.text = "Week " + data.week + ", " + data.day + ", 6:00 PM";
            }
            savePanel.Find("SaveDate").GetComponent<Text>().text = "SAVED : " + SaveManager.getSaveDate(currentSaveSelected);
            float Atmosphere = 10 - data.studentsKilled;
            Color CloudColor = savePanel.Find("Clouds").GetComponent<Image>().color;
            CloudColor.a = 0f + ((255f * (data.studentsKilled / 10f)) / 255);
            savePanel.Find("Clouds").GetComponent<Image>().color = CloudColor;
            Color newColor = new Color(0.3f, 0.3f, 0.3f);
            Color newColor1 = new Color(1f, 1f, 1f);
            savePanel.Find("Button1").GetComponent<Text>().color = newColor1;
            savePanel.Find("Button2").GetComponent<Text>().color = newColor;
            savePanel.Find("Button3").GetComponent<Text>().color = newColor1;
        }
        else if (SaveManager.saveExists(saveID) == saveStatus.Corrupt)
        {
            savePanel.Find("SaveName").GetComponent<Text>().text = "CORRUPT SAVE";
            savePanel.Find("SaveInfo").GetComponent<Text>().text = "N/A";
            Color CloudColor = savePanel.Find("Clouds").GetComponent<Image>().color;
            CloudColor.a = 1;
            savePanel.Find("Clouds").GetComponent<Image>().color = CloudColor;
            Color newColor = new Color(0.3f, 0.3f, 0.3f);
            Color newColor1 = new Color(1f, 1f, 1f);
            savePanel.Find("Button1").GetComponent<Text>().color = newColor;
            savePanel.Find("Button2").GetComponent<Text>().color = newColor1;
            savePanel.Find("Button3").GetComponent<Text>().color = newColor;
        }
        else if (SaveManager.saveExists(saveID) == saveStatus.Nonexistant)
        {
            savePanel.Find("SaveName").GetComponent<Text>().text = "EMPTY FILE";
            savePanel.Find("SaveInfo").GetComponent<Text>().text = "Week 0, Monday, 1:00 AM";
            savePanel.Find("SaveDate").GetComponent<Text>().text = "SAVED : 01/01/01";
            Color newColor = new Color(0.3f, 0.3f, 0.3f);
            Color newColor1 = new Color(1f, 1f, 1f);
            savePanel.Find("Button1").GetComponent<Text>().color = newColor;
            savePanel.Find("Button2").GetComponent<Text>().color = newColor1;
            savePanel.Find("Button3").GetComponent<Text>().color = newColor;
        }
    }
}
