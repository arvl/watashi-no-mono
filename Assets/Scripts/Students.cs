﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Students : MonoBehaviour {
    public Destination[] goAwaySpots;
    public Student[] allStudents;

    // Initiate
    void Start()
    {
        allStudents = GameObject.FindObjectsOfType<Student>();
    }
}
