﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    public Vector3 rotateDelta;
    void Update()
    {
        transform.Rotate(rotateDelta * Time.deltaTime);
    }
}